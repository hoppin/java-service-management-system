package com.hoppinzq;

import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

public class RedisDemo {

    JedisPool jedisPool=null;
    public RedisDemo() {
        jedisPool = new JedisPool(new JedisPoolConfig(), "",
                6379,10000,"");
    }

    public JedisPool getJedisPool() {
        return jedisPool;
    }

    public void setJedisPool(JedisPool jedisPool) {
        this.jedisPool = jedisPool;
    }

    public void close(){
        jedisPool.close();
    }

}
