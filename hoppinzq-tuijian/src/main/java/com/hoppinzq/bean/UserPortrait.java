package com.hoppinzq.bean;

import java.util.List;

/**
 * 模拟：用户画像表
 */
public class UserPortrait {
    private User user;
    private List<String> user_prefer;

    public UserPortrait(User user, List<String> user_prefer) {
        this.user = user;
        this.user_prefer = user_prefer;
    }


    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserPortrait() {
    }

    public List<String> getUser_prefer() {
        return user_prefer;
    }

    public void setUser_prefer(List<String> user_prefer) {
        this.user_prefer = user_prefer;
    }
}
