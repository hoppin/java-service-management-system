package com.hoppinzq.bean;

/**
 * 模拟：用户视频行为表
 */
public class UserVideoAction {
    private User user;
    public Action action;
    private Video video;
    private String create;

    public UserVideoAction() {
    }

    public UserVideoAction(User user, Action action, com.hoppinzq.bean.Video video, String create) {
        this.user = user;
        this.action = action;
        this.video = video;
        this.create = create;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Video getVideo() {
        return video;
    }

    public void setVideo(Video video) {
        this.video = video;
    }

    public String getCreate() {
        return create;
    }

    public void setCreate(String create) {
        this.create = create;
    }
}
