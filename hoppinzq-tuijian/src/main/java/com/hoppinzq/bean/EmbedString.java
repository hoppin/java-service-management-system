package com.hoppinzq.bean;

import java.util.Arrays;

public class EmbedString {
    private String content;
    public double[] embedding;
    public double cosineSimilarity;

    @Override
    public String toString() {
        return "EmbedString{" +
                "content='" + content + '\'' +
                ", cosineSimilarity=" + cosineSimilarity +
                '}';
    }

    public EmbedString() {
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public double[] getEmbedding() {
        return embedding;
    }

    public void setEmbedding(double[] embedding) {
        this.embedding = embedding;
    }

    public double getCosineSimilarity() {
        return cosineSimilarity;
    }

    public void setCosineSimilarity(double cosineSimilarity) {
        this.cosineSimilarity = cosineSimilarity;
    }

    public EmbedString(String content, double[] embedding, double cosineSimilarity) {
        this.content = content;
        this.embedding = embedding;
        this.cosineSimilarity = cosineSimilarity;
    }
}
