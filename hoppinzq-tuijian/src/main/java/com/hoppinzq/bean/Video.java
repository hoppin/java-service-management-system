package com.hoppinzq.bean;

/**
 * 模拟：视频表
 */
public class Video {
    private String video_id;
    private String video_name;
    private String video_miaoshu;
    private String video_class;

    public Video() {
    }

    public Video(String video_id, String video_name, String video_miaoshu, String video_class) {
        this.video_id = video_id;
        this.video_name = video_name;
        this.video_miaoshu = video_miaoshu;
        this.video_class = video_class;
    }

    public String getVideo_class() {
        return video_class;
    }

    public void setVideo_class(String video_class) {
        this.video_class = video_class;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_miaoshu() {
        return video_miaoshu;
    }

    public void setVideo_miaoshu(String video_miaoshu) {
        this.video_miaoshu = video_miaoshu;
    }

}
