package com.hoppinzq.bean;

/**
 * 模拟：行为表
 */
public class Action {
    private String action_id;
    private String action_type;
    private String action_open;
    private String action_close;
    private String action_duration;

    public Action() {
    }

    public Action(String action_id,String action_type, String action_open, String action_close, String action_duration) {
        this.action_id = action_id;
        this.action_type = action_type;
        this.action_open = action_open;
        this.action_close = action_close;
        this.action_duration = action_duration;
    }

    public String getAction_id() {
        return action_id;
    }

    public void setAction_id(String action_id) {
        this.action_id = action_id;
    }

    public String getAction_type() {
        return action_type;
    }

    public void setAction_type(String action_type) {
        this.action_type = action_type;
    }

    public String getAction_open() {
        return action_open;
    }

    public void setAction_open(String action_open) {
        this.action_open = action_open;
    }

    public String getAction_close() {
        return action_close;
    }

    public void setAction_close(String action_close) {
        this.action_close = action_close;
    }

    public String getAction_duration() {
        return action_duration;
    }

    public void setAction_duration(String action_duration) {
        this.action_duration = action_duration;
    }
}
