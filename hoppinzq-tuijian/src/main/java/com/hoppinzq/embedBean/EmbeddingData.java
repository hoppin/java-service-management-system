package com.hoppinzq.embedBean;

import java.util.Arrays;

public class EmbeddingData {
    private String object;
    private double[] embedding;
    private int index;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public double[] getEmbedding() {
        return embedding;
    }

    public void setEmbedding(double[] embedding) {
        this.embedding = embedding;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "EmbeddingData{" +
                "object='" + object + '\'' +
                ", embedding=" + embedding[0] +"...省略"+
                ", index=" + index +
                '}';
    }
}
