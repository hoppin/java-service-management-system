package com.hoppinzq.embedBean;

import java.util.Arrays;

public class Embed {
    private String model;
    private String object;
    public EmbeddingData[] data;
    private Usage usage;

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public EmbeddingData[] getData() {
        return data;
    }

    public void setData(EmbeddingData[] data) {
        this.data = data;
    }

    public Usage getUsage() {
        return usage;
    }

    public void setUsage(Usage usage) {
        this.usage = usage;
    }

    @Override
    public String toString() {
        return "Embed{" +
                "model='" + model + '\'' +
                ", object='" + object + '\'' +
                ", data=" + Arrays.toString(data) +
                ", usage=" + usage +
                '}';
    }
}
