package com.hoppinzq.service.aop.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface RateLimit {
    int limit() default 5; // 默认的限流阈值
    long timeout() default 1000; // 默认的限流时间间隔，单位为毫秒
}