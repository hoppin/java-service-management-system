# java服务管理系统

#### 介绍
从我的首页的剑来框架开源项目复制进来的（主要是懒，都不会新建maven项目了），项目在 hoppinzq-manager模块下

#### 说明

这个项目配置文件，数据库sql脚本已提交（在resources文件下）。但还有文件系统，索引库位置，前后端分离项目前端，还有各种api的key都没提交，若想要跑起项目，微信:HOPPIN_HAZZ
<p align='center'>
  <img src="http://hoppinzq.com/static/images/logo/1640338711_113639.png" width='400'/>
</p>

<p align='center'>
  <a href='https://hoppinzq.com/manager/index.html'>
    <img src='https://img.shields.io/badge/-hoppinzq-green?logo=hoppinzq&logoColor=white&color=green'/>
  </a>
</p>

<br>

## 🎤介绍
重要：chatGPT模块的resource内有个压缩文件，是chatgpt项目从本项目中拆出去的，若想只用chatgpt模块，解压这个压缩文件就行  
另外，项目跑不起来请加我微信HOPPIN_HAZZ，我很久没提交代码了，增加了若干功能Key池之类的，加我微信获取最新代码（主要是个人提交习惯不好，而且懒得维护😀）

访问[https://hoppinzq.com/manager/index.html](https://hoppinzq.com/manager/index.html)
## ✏️我的博客

- [✨自己写的博客网站✨](http://hoppin.cn/)

## ✏️我的视频

- [✨自己写的视频网站✨](https://hoppinzq.com/video/index.html)
##  🎬反馈
>  🌷你可以发起 Issue

##  😘感谢
[apitest](https://gitee.com/hoppin/apitest)

##  📄License

MIT


