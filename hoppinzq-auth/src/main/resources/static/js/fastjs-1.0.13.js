/*
* Fastjs Javascript Frame
*
* About this frame:
*   Version:v1.0.13
*   Author:XiaoDong Team-XiaoDong (xiaodong@indouyin.cn)
*   Contact-Us: xiaodong@indouyin.cn
*   Follow-Us: https://leetcode-cn.com/u/dy_xiaodong/
*              https://juejin.cn/user/3540872450829720/
*
* Frame license:
*   MIT License
*/

const FASTJS_CONFIG = {
    "version": "1.0.13",
    "newVersionCheck": true,
    "css": {
        "setup": true,
    },
    "error": {
        "output": {
            "smallErrorOutput": true,
            "errorOutput": true,
            "seriousErrorOutput": true,
            "crashErrorOutput": "always on"
        },
        "ajax": {
            "urlNotSecurity": true,
            "checkTimeout": true
        },
        "other": {
            "outOfRange": true
        }
    },
    "log": {
        "fastjsInstallLog": true,
        "ajaxLog": true
    },
    "compiler": {
        "qualityInspection": true
    },
    "function": {
        "Class": {
            "dom": {
                "defaultTagName": "div"
            }
        },
        "String": {
            "push": {
                match: "%s%"
            }
        }
    },
    "globalFunction": {
        "getcookie": true,
        "setcookie": true,
        "random": true,
        "md5": true
    }
};

class dom {
    constructor(tagname, element) {
        let el = document.createElement(tagname ? tagname : FASTJS_CONFIG.function.Class.dom.defaultTagName);
        if (element && element.length > 1) {
            if (element[0] === ".") {
                el.className = element.substr(1);
            }
            if (element[0] === "#") {
                el.id = element.substr(1);
            }
        }
        return el;
    }
}
class ajax {
    constructor(url, data, callback, timeout, datatype) {
        switch (timeout) {
            case undefined:
                timeout = 5000;
                break;
            case timeout < 1000:
                if (FASTJS_CONFIG.error.ajax.checkTimeout) {
                    fastjs.qualityOutput("Class.ajax.__constructor__", "Illogical timeout");
                }
        }
        switch (datatype) {
            case undefined:
                datatype = "auto";
                break;
            case!["json", "text"].search(datatype):
                datatype = "auto";
                fastjs.throwError("Class.ajax.__constructor__", "Unknown type given of datatype");
        }
        this.config = {
            url: url,
            data: data,
            callback: callback,
            timeout: timeout
        }
    }
    send(url, data, callback, method) {
        if (this.url) {
            this.config.url = this.url;
        }
        if (this.data) {
            this.config.data = this.data;
        }
        if (this.callback) {
            this.config.callback = this.callback;
        }
        if (this.timeout) {
            this.config.timeout = this.timeout;
        }
        if (!url) {
            if (!this.config.url) {
                fastjs.throwCrashError("Class.ajax.%s%->send".push(method), "Empty data given of url, but it should had something");
            }
            url = this.config.url;
        }
        let timeout = this.config.timeout;
        if (!callback) {
            if (!this.config.callback) {
                this.config.callback = () => {
                };
            }
            callback = this.config.callback;
        }
        let datatype = this.datatype;
        if (!data) {
            data = this.config.data;
        }
        let info = "";
        if (data) {
            data.forEach((e, key) => {
                info += e[0] + "=" + e[1] + (key === 0 ? "" : "&");
            });
        }
        if (url.search("http://") !== -1) {
            if (FASTJS_CONFIG.error.ajax.urlNotSecurity) {
                fastjs.throwSmallError("Class.ajax.%s%->send".push(method), "The url given is not secure");
            }
        }
        let xhr = new XMLHttpRequest();
        if (datatype === "auto" || datatype === "text") {
            xhr.responseType = "text";
        } else {
            xhr.responseType = "json";
        }
        xhr.timeout = timeout;
        xhr.sendTimeout = setTimeout(() => {
            if (FASTJS_CONFIG.log.ajaxLog) {
                console.log("[Fastjs ajax] ajaxRequest to url %s is failed".replace("%s", url));
            }
            if (this.callback && typeof this.callback == "function") {
                this.callback("!failed", xhr.readyState);
            }
            this.result = "!failed";
        }, timeout);
        xhr.open(method === "get" ? "get" : "post", method === "get" ? url + "?" + info : url);
        if (method === "post") {
            xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        }
        xhr.onload = () => {
            clearTimeout(xhr.sendTimeout);
            let response = xhr.response;
            if (datatype === "auto") {
                try {
                    response = JSON.parse(response);
                } catch (e) {
                }
            }
            this.result = response;
            this.status = xhr.status;
            if (FASTJS_CONFIG.log.ajaxLog) {
                console.log("[Fastjs ajax] ajaxRequest to url %s is success".replace("%s", url));
            }
            if (callback && typeof callback == "function") {
                callback(response, 200);
            }
        };
        xhr.send(method === "post" ? info : null);
        this.xhr = xhr;
        return true;
    }
    post(url, data, callback) {
        this.send(url, data, callback, "post");
    }
    get(url, data, callback) {
        this.send(url, data, callback, "get");
    }
}
class fastjs {
    static setup() {
        window.js = this;
        DOMTokenList.prototype.push = function ($list) {
            let el = "";
            if (typeof $list === "object") {
                $list.forEach((e, key) => {
                    el += e;
                    el += key + 1 !== $list.length ? " " : ""
                })
            } else if (typeof $list === "string") {
                el = $list;
            } else {
                fastjs.throwError("DOMTokenList.push", "unknown type given of $list");
            }
            el = $list.split(" ");
            el.forEach((e) => {
                this.add(e);
            });
            return this;
        };
        Element.prototype.addClass = function ($list) {
            let el = this;
            if (typeof $list !== "string") {
                fastjs.throwError("Element.addClass", "unknown type given of $list");
                return this;
            }
            $list = $list.split(" ");
            $list.forEach((name) => {
                el.classList.add(name);
            });
            return this;
        };
        Element.prototype.css = function ($css) {
            let css = "";
            if (typeof $css === "string") {
                css = $css;
            } else if (typeof $css === "object") {
                $css.forEach((e) => {
                    css += "%s%: %s%;".push([e[0], e[1]]);
                })
            }
            this.style = css;
            return this;
        };
        Element.prototype.insertAfter = function ($dom) {
            let par = $dom.parentNode;
            if (par.lastChild === $dom) {
                par.appendChild(this);
            } else {
                par.insertBefore(this, $dom.nextSibling);
            }
            return this;
        };
        Element.prototype.attr = function ($name, $value) {
            return fastjs.attr($name, $value, this);
        };
        Element.prototype.event_click = function ($event) {
            fastjs.on($event, this, "onclick");
            return this;
        };
        Element.prototype.event_blur = function ($event) {
            fastjs.on($event, this, "onblur");
            return this;
        };
        Element.prototype.event_focus = function ($event) {
            fastjs.on($event, this, "onfocus");
            return this;
        };
        Element.prototype.event_change = function ($event) {
            fastjs.on($event, this, "onchange");
            return this;
        };
        Element.prototype.event_input = function ($event) {
            fastjs.on($event, this, "oninput");
            return this;
        };
        Element.prototype.event_keyup = function ($event) {
            fastjs.on($event, this, "onkeyup");
            return this;
        };
        Element.prototype.event_keydown = function ($event) {
            fastjs.on($event, this, "onkeydown");
            return this;
        };
        Element.prototype.domAddEnd = function ($domadd) {
            fastjs.domAddEnd($domadd, this);
            return this;
        };
        Element.prototype.html = function ($html) {
            if (!$html) {
                $html = "";
            }
            return this.innerHTML = $html;
        };
        Element.prototype.text = function ($text) {
            if (!$text) {
                $text = "";
            }
            return this.innerText = $text;
        };
        Element.prototype.gethtml = function () {
            return this.innerHTML;
        };
        Element.prototype.gettext = function () {
            return this.innerText;
        };
        Element.prototype.next = function (selecter) {
            return fastjs.dom(selecter, this);
        };
        Element.prototype.then = function (callback) {
            if (callback === "string") {
                eval(callback);
            } else {
                callback(this);
            }
            return this;
        };
        Element.prototype.val = function () {
            return this.tagName === "input" ? this.value : this.html();
        };
        Element.prototype.log = function () {
            console.log(this);
            return this;
        };
        Element.prototype.pull = function ($selecter) {
            let dom = document.querySelector($selecter);
            if (!dom) {
                fastjs.throwError("Element.pull", "selecter is empty");
                return this;
            }
            dom.appendChild(this);
            return this;
        };
        Element.prototype.cover = function ($selecter) {
            let dom = document.querySelector($selecter);
            if (!dom) {
                fastjs.throwError("Element.cover", "selecter is empty");
                return this;
            }
            dom.html();
            dom.appendChild(this);
            return this;
        };
        window.$ = function dom($selecter) {
            return fastjs.dom($selecter);
        };
        String.prototype.push = function ($strings, $target) {
            let str = this;
            if (!$strings) {
                $strings = "";
            }
            if (typeof $strings === "string") {
                str = str.replaceAll(FASTJS_CONFIG.function.String.push.match, $target !== undefined ? $target : $strings);
            } else if (typeof $strings === "object") {
                $strings.forEach((e, key) => {
                    if (key >= $strings.length) {
                        e = $strings[$strings.length - 1];
                        if (FASTJS_CONFIG.error.other.outOfRange) {
                            fastjs.throwSmallError("String.push", "List out of range");
                        }
                    }
                    str = str.replace("%s%", e);
                })
            }
            return str;
        };
        String.prototype.search = function ($keyword) {
            let str = this;
            str = str.indexOf($keyword);
            if (!str) {
                str = false;
            }
            return str;
        };
        String.prototype.log = function () {
            console.log(this);
            return this;
        };
        String.prototype.strrev = function () {
            return this.split("").reverse().join("");
        };
        String.prototype.pushToDom = function ($selecter) {
            let el = document.querySelector($selecter);
            if (!el) {
                fastjs.throwError("String.pushToDom", "selecter is empty");
                return this;
            }
            el.html(this);
            return this;
        };
        String.prototype.calc = function () {
            return this.match(/[0-9*+%\-\/]*$/) ? eval(this) : false;
        };
        String.prototype.array = function ($keyword) {
            if (typeof $keyword === "string") {
                let str = "";
                let ar = [];
                for (let i = 0; i < this.length; i++) {
                    console.log(str);
                    str += this[i];
                    if (str.search($keyword) !== -1) {
                        str = str.replace($keyword, "");
                        ar.push(str);
                        str = "";
                    }
                }
                if (str.length) {
                    ar.push(str);
                }
                return ar;
            } else if (typeof $keyword === "number") {
                let time = 0;
                let str = "";
                let ar = [];
                for (let i = 0; i < this.length; i++) {
                    time++;
                    str += this[i];
                    if (time === $keyword) {
                        ar.push(str);
                        str = "";
                        time = 0;
                    }
                }
                return ar;
            }
            fastjs.throwCrashError("String.array", "Unknown type given of $keyword");
        };
        Array.prototype.search = function ($keyword) {
            let el = this;
            if (typeof $keyword === "string") {
            } else if (typeof $keyword === "number") {
                $keyword = String($keyword);
            } else {
                fastjs.throwError("Array.search", "unknown type given of $keyword");
                return false;
            }
            el = el.indexOf($keyword) ? el.indexOf($keyword) : false;
            return el;
        };
        Array.prototype.random = function () {
            let length = this.length;
            length--;
            let upper = length;
            let lower = 0;
            let random = Math.floor(Math.random() * (upper - lower + 1)) + lower;
            return this[random];
        };
        Array.prototype.add = function ($index, $key) {
            if (!$key && $key !== 0) {
                this.push($index);
            } else {
                if (this.length - 1 > $key) {
                    fastjs.throwCrashError("Array.add", "key is out of range");
                } else if ($key < 0) {
                    fastjs.throwCrashError("Array.add", "key is out of range");
                }
            }
            this.splice($key, 0, $index);
            return this;
        };
        Array.prototype.delete = function ($key, $num) {
            if (!$num) {
                if ($num === 0) {
                    fastjs.qualityOutput("Array.delete", "Method calls that could have been ignored");
                    return false;
                }
                $num = 1;
            }
            this.splice($key, $num);
            return this;
        };
        Array.prototype.resort = function () {
            return this.sort().reverse();
        };
        Array.prototype.log = function () {
            console.log(this);
            return this;
        };
        Array.prototype.string = function ($keyword, $custom) {
            if (!$keyword) {
                $keyword = ",";
            }
            let str = "";
            this.forEach((e, key) => {
                str += "%s%%s%".push([e, key + 1 === this.length ? "" : $keyword]);
            });
            return $custom ? "[%s%]".push(str) : str;
        };
        Object.prototype.log = function () {
            console.log(this);
            return this;
        };
        if (FASTJS_CONFIG.log.fastjsInstallLog) {
            console.log("Fastjs v%s already install successfully!".replaceAll("%s", FASTJS_CONFIG.version));
        }
        if (FASTJS_CONFIG.globalFunction.getcookie) {
            window.getcookie = function (cookie) {
                return fastjs.getcookie(cookie);
            }
        }
        if (FASTJS_CONFIG.globalFunction.setcookie) {
            window.setcookie = function (name, value, sec, domain) {
                return fastjs.setcookie(name, value, sec, domain);
            }
        }
        if (FASTJS_CONFIG.globalFunction.random) {
            window.random = function ($lower, $upper) {
                return fastjs.random($lower, $upper);
            }
        }
        if (FASTJS_CONFIG.globalFunction.md5) {
            window.md5 = function ($text) {
                return fastjs.md5($text);
            }
        }
        if (FASTJS_CONFIG.css.setup) {
            let css_el = new dom("style");
            css_el.html("body{margin: 0;font-family:-apple-system,BlinkMacSystemFont,\"SegoeUI\",Roboto,\"HelveticaNeue\",Arial,sans-serif,\"AppleColorEmoji\",\"SegoeUIEmoji\",\"SegoeUISymbol\",\"NotoColorEmoji\";}*{box-sizing:border-box;}input:focus{outline:none}button:focus{outline:none;}a{text-decoration-line:none;color:unset;}a:hover{text-decoration-line:none;color:unset;}");
            document.head.domAddEnd(css_el);
        }
        if (FASTJS_CONFIG.newVersionCheck) {
            let versionCheck = new ajax("https://fastjs.com.cn/lastVersion.php");
            versionCheck.callback = result => {
                if (result.version !== FASTJS_CONFIG.version && result !== "!failed") {
                    console.log("[Fastjs] Fastjs have a new version %s% can update, go to https://fastjs.com.cn/ to get more info".push(result.version));
                }
            };
            versionCheck.get();
        }
    }
    static random($lower, $upper) {
        if ($upper <= $lower) {
            fastjs.throwCrashError("main.random", "$lower and $upper is not standard");
        }
        if (typeof $lower !== "number" || typeof $upper !== "number") {
            fastjs.throwCrashError("main.random", "unknown type given if $lower or $upper");
        }
        return Math.floor(Math.random() * ($upper - $lower + 1)) + $lower;
    }
    static throwSmallError($place, $text) {
        if (FASTJS_CONFIG.error.output.smallErrorOutput) {
            let output = "[Fastjs Error] Fastjs.%s%.smallError: %s%".push([$place, $text]);
            console.warn(output);
        }
    }
    static throwError($place, $text) {
        if (FASTJS_CONFIG.error.output.errorOutput) {
            let output = "[Fastjs Error] Fastjs.%s%.error: %s%".push([$place, $text]);
            console.warn(output);
        }
    }
    static throwSeriousError($place, $text) {
        if (FASTJS_CONFIG.error.output.seriousErrorOutput) {
            let output = "[Fastjs Error] Fastjs.%s%.seriousError: %s%".push([$place, $text]);
            console.warn(output);
        }
    }
    static throwCrashError($place, $text) {
        if (FASTJS_CONFIG.error.output.crashErrorOutput) {
            throw"Fastjs.%s%.crashError: %s%".push([$place, $text]);
        }
    }
    static qualityOutput($place, $text) {
        if (FASTJS_CONFIG.compiler.qualityInspection) {
            let output = "[Fastjs quality inspection] Fastjs.%s%.qualityInspection: %s%".push([$place, $text]);
            console.log(output);
        }
    }
    static domAddEnd($domadd, $place) {
        $place.appendChild($domadd);
    }
    static dom($selecter, $element) {
        if (!$element) {
            $element = document;
        }
        let selecter = [];
        let string = [];
        for (let i = 0; i < $selecter.length; i++) {
            string.push($selecter[i]);
        }
        let selecting = false;
        let selecting_option = null;
        let selecting_name = "";
        string.forEach((e) => {
            if (e === "." || e === "#" || e === "?") {
                if (selecting) {
                    selecter.push(
                        {
                            type: selecting_option,
                            index: selecting_name
                        }
                    )
                }
                selecting_name = "";
                selecting = true;
                if (e === ".") {
                    selecting_option = "class";
                } else if (e === "#") {
                    selecting_option = "id";
                } else {
                    selecting_option = "tag";
                }
                selecting_name += e;
            } else {
                selecting_name += e;
            }
        });
        selecter.push(
            {
                type: selecting_option,
                index: selecting_name
            }
        );
        let result = null;
        selecter.forEach((e, key) => {
            if (e.type === "tag" && e.index[0] === "?") {
                e.index = e.index.slice(1);
            }
            if (key !== selecter.length - 1) {
                if (!key) {
                    result = $element.querySelector(e.index);
                } else {
                    result = result.querySelector(e.index);
                }
            } else {
                if (e.type === "id" || e.index === "body" || e.index === "html" || e.index === "head") {
                    if (result) {
                        result = result.querySelector(e.index);
                    } else {
                        result = $element.querySelector(e.index);
                    }
                } else {
                    if (result) {
                        result = result.querySelectorAll(e.index);
                    } else {
                        result = $element.querySelectorAll(e.index);
                    }
                }
            }
        });
        return result;
    }
    static on($event, $dom, $name) {
        if (typeof $event == "function") {
            $dom[$name] = () => {
                $event();
            }
        } else if (typeof $event == "string") {
            fastjs.qualityOutput("main.on", "We recommend you use function type to give a javascript");
            $dom[$name] = () => {
                eval($event);
            }
        } else {
            if (!$event) {
                fastjs.qualityOutput("main.on", "You don't need to use this function if $event is null");
                return false;
            }
            if ($event === "" || $event.length === 0) {
                fastjs.qualityOutput("main.on", "You don't need to use this function if $event is null");
                return false;
            }
            $dom[$name] = () => {
                $event.forEach(($function) => {
                    if (typeof $function == "function") {
                        $function();
                    } else {
                        eval($function);
                    }
                })
            }
        }
        return $dom;
    }
    static attr($name, $value, $dom) {
        if (!$dom) {
            return $dom;
        }
        if (typeof $value == "number") {
            $value += "";
        }
        if (!$value && typeof ($value) == "boolean") {
            return $dom.getAttribute($name);
        } else if (typeof ($value) != "string") {
            if (!$dom.getAttribute($name)) {
                return $dom;
            }
            $dom.removeAttribute($name);
            return $dom;
        } else {
            $dom.setAttribute($name, $value);
            return $dom;
        }
    };
    static urlget(url, data, callback, datatype) {
        fastjs.qualityOutput("main.urlget", "fastjs.urlget is not support after v1.0.11");
    };
    static urlpost(url, data, callback, datatype) {
        fastjs.qualityOutput("main.urlget", "fastjs.urlpost is not support after v1.0.11");
    };
    static copy(data) {
        var oInput = document.createElement('input');
        oInput.value = data;
        document.body.appendChild(oInput);
        oInput.select();
        document.execCommand("Copy");
        oInput.className = 'oInput';
        oInput.style.display = 'none';
        oInput.remove();
    };
    static md5($text) {
        function RotateLeft(lValue, iShiftBits) {
            return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
        }
        function AddUnsigned(lX, lY) {
            var lX4, lY4, lX8, lY8, lResult;
            lX8 = (lX & 0x80000000);
            lY8 = (lY & 0x80000000);
            lX4 = (lX & 0x40000000);
            lY4 = (lY & 0x40000000);
            lResult = (lX & 0x3FFFFFFF) + (lY & 0x3FFFFFFF);
            if (lX4 & lY4) {
                return (lResult ^ 0x80000000 ^ lX8 ^ lY8);
            }
            if (lX4 | lY4) {
                if (lResult & 0x40000000) {
                    return (lResult ^ 0xC0000000 ^ lX8 ^ lY8);
                } else {
                    return (lResult ^ 0x40000000 ^ lX8 ^ lY8);
                }
            } else {
                return (lResult ^ lX8 ^ lY8);
            }
        }
        function F(x, y, z) {
            return (x & y) | ((~x) & z);
        }
        function G(x, y, z) {
            return (x & z) | (y & (~z));
        }
        function H(x, y, z) {
            return (x ^ y ^ z);
        }
        function I(x, y, z) {
            return (y ^ (x | (~z)));
        }
        function FF(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(F(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        function GG(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(G(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        function HH(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(H(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        function II(a, b, c, d, x, s, ac) {
            a = AddUnsigned(a, AddUnsigned(AddUnsigned(I(b, c, d), x), ac));
            return AddUnsigned(RotateLeft(a, s), b);
        }
        function ConvertToWordArray(sMessage) {
            var lWordCount;
            var lMessageLength = sMessage.length;
            var lNumberOfWords_temp1 = lMessageLength + 8;
            var lNumberOfWords_temp2 = (lNumberOfWords_temp1 - (lNumberOfWords_temp1 % 64)) / 64;
            var lNumberOfWords = (lNumberOfWords_temp2 + 1) * 16;
            var lWordArray = Array(lNumberOfWords - 1);
            var lBytePosition = 0;
            var lByteCount = 0;
            while (lByteCount < lMessageLength) {
                lWordCount = (lByteCount - (lByteCount % 4)) / 4;
                lBytePosition = (lByteCount % 4) * 8;
                lWordArray[lWordCount] = (lWordArray[lWordCount] | (sMessage.charCodeAt(lByteCount) << lBytePosition));
                lByteCount++;
            }
            lWordCount = (lByteCount - (lByteCount % 4)) / 4;
            lBytePosition = (lByteCount % 4) * 8;
            lWordArray[lWordCount] = lWordArray[lWordCount] | (0x80 << lBytePosition);
            lWordArray[lNumberOfWords - 2] = lMessageLength << 3;
            lWordArray[lNumberOfWords - 1] = lMessageLength >>> 29;
            return lWordArray;
        }
        function WordToHex(lValue) {
            var WordToHexValue = "", WordToHexValue_temp = "", lByte, lCount;
            for (lCount = 0; lCount <= 3; lCount++) {
                lByte = (lValue >>> (lCount * 8)) & 255;
                WordToHexValue_temp = "0" + lByte.toString(16);
                WordToHexValue = WordToHexValue + WordToHexValue_temp.substr(WordToHexValue_temp.length - 2, 2);
            }
            return WordToHexValue;
        }
        var x;
        var k, AA, BB, CC, DD, a, b, c, d
        var S11 = 7, S12 = 12, S13 = 17, S14 = 22;
        var S21 = 5, S22 = 9, S23 = 14, S24 = 20;
        var S31 = 4, S32 = 11, S33 = 16, S34 = 23;
        var S41 = 6, S42 = 10, S43 = 15, S44 = 21;
        x = ConvertToWordArray($text);
        a = 0x67452301;
        b = 0xEFCDAB89;
        c = 0x98BADCFE;
        d = 0x10325476;
        for (k = 0; k < x.length; k += 16) {
            AA = a;
            BB = b;
            CC = c;
            DD = d;
            a = FF(a, b, c, d, x[k], S11, 0xD76AA478);
            d = FF(d, a, b, c, x[k + 1], S12, 0xE8C7B756);
            c = FF(c, d, a, b, x[k + 2], S13, 0x242070DB);
            b = FF(b, c, d, a, x[k + 3], S14, 0xC1BDCEEE);
            a = FF(a, b, c, d, x[k + 4], S11, 0xF57C0FAF);
            d = FF(d, a, b, c, x[k + 5], S12, 0x4787C62A);
            c = FF(c, d, a, b, x[k + 6], S13, 0xA8304613);
            b = FF(b, c, d, a, x[k + 7], S14, 0xFD469501);
            a = FF(a, b, c, d, x[k + 8], S11, 0x698098D8);
            d = FF(d, a, b, c, x[k + 9], S12, 0x8B44F7AF);
            c = FF(c, d, a, b, x[k + 10], S13, 0xFFFF5BB1);
            b = FF(b, c, d, a, x[k + 11], S14, 0x895CD7BE);
            a = FF(a, b, c, d, x[k + 12], S11, 0x6B901122);
            d = FF(d, a, b, c, x[k + 13], S12, 0xFD987193);
            c = FF(c, d, a, b, x[k + 14], S13, 0xA679438E);
            b = FF(b, c, d, a, x[k + 15], S14, 0x49B40821);
            a = GG(a, b, c, d, x[k + 1], S21, 0xF61E2562);
            d = GG(d, a, b, c, x[k + 6], S22, 0xC040B340);
            c = GG(c, d, a, b, x[k + 11], S23, 0x265E5A51);
            b = GG(b, c, d, a, x[k], S24, 0xE9B6C7AA);
            a = GG(a, b, c, d, x[k + 5], S21, 0xD62F105D);
            d = GG(d, a, b, c, x[k + 10], S22, 0x2441453);
            c = GG(c, d, a, b, x[k + 15], S23, 0xD8A1E681);
            b = GG(b, c, d, a, x[k + 4], S24, 0xE7D3FBC8);
            a = GG(a, b, c, d, x[k + 9], S21, 0x21E1CDE6);
            d = GG(d, a, b, c, x[k + 14], S22, 0xC33707D6);
            c = GG(c, d, a, b, x[k + 3], S23, 0xF4D50D87);
            b = GG(b, c, d, a, x[k + 8], S24, 0x455A14ED);
            a = GG(a, b, c, d, x[k + 13], S21, 0xA9E3E905);
            d = GG(d, a, b, c, x[k + 2], S22, 0xFCEFA3F8);
            c = GG(c, d, a, b, x[k + 7], S23, 0x676F02D9);
            b = GG(b, c, d, a, x[k + 12], S24, 0x8D2A4C8A);
            a = HH(a, b, c, d, x[k + 5], S31, 0xFFFA3942);
            d = HH(d, a, b, c, x[k + 8], S32, 0x8771F681);
            c = HH(c, d, a, b, x[k + 11], S33, 0x6D9D6122);
            b = HH(b, c, d, a, x[k + 14], S34, 0xFDE5380C);
            a = HH(a, b, c, d, x[k + 1], S31, 0xA4BEEA44);
            d = HH(d, a, b, c, x[k + 4], S32, 0x4BDECFA9);
            c = HH(c, d, a, b, x[k + 7], S33, 0xF6BB4B60);
            b = HH(b, c, d, a, x[k + 10], S34, 0xBEBFBC70);
            a = HH(a, b, c, d, x[k + 13], S31, 0x289B7EC6);
            d = HH(d, a, b, c, x[k], S32, 0xEAA127FA);
            c = HH(c, d, a, b, x[k + 3], S33, 0xD4EF3085);
            b = HH(b, c, d, a, x[k + 6], S34, 0x4881D05);
            a = HH(a, b, c, d, x[k + 9], S31, 0xD9D4D039);
            d = HH(d, a, b, c, x[k + 12], S32, 0xE6DB99E5);
            c = HH(c, d, a, b, x[k + 15], S33, 0x1FA27CF8);
            b = HH(b, c, d, a, x[k + 2], S34, 0xC4AC5665);
            a = II(a, b, c, d, x[k], S41, 0xF4292244);
            d = II(d, a, b, c, x[k + 7], S42, 0x432AFF97);
            c = II(c, d, a, b, x[k + 14], S43, 0xAB9423A7);
            b = II(b, c, d, a, x[k + 5], S44, 0xFC93A039);
            a = II(a, b, c, d, x[k + 12], S41, 0x655B59C3);
            d = II(d, a, b, c, x[k + 3], S42, 0x8F0CCC92);
            c = II(c, d, a, b, x[k + 10], S43, 0xFFEFF47D);
            b = II(b, c, d, a, x[k + 1], S44, 0x85845DD1);
            a = II(a, b, c, d, x[k + 8], S41, 0x6FA87E4F);
            d = II(d, a, b, c, x[k + 15], S42, 0xFE2CE6E0);
            c = II(c, d, a, b, x[k + 6], S43, 0xA3014314);
            b = II(b, c, d, a, x[k + 13], S44, 0x4E0811A1);
            a = II(a, b, c, d, x[k + 4], S41, 0xF7537E82);
            d = II(d, a, b, c, x[k + 11], S42, 0xBD3AF235);
            c = II(c, d, a, b, x[k + 2], S43, 0x2AD7D2BB);
            b = II(b, c, d, a, x[k + 9], S44, 0xEB86D391);
            a = AddUnsigned(a, AA);
            b = AddUnsigned(b, BB);
            c = AddUnsigned(c, CC);
            d = AddUnsigned(d, DD);
        }
        var temp = WordToHex(a) + WordToHex(b) + WordToHex(c) + WordToHex(d);
        return temp.toLowerCase();
    };
    static getcookie(cookie) {
        var name = cookie + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i].trim();
            if (c.indexOf(name) === 0) return c.substring(name.length, c.length);
        }
        return "";
    }
    static setcookie(name, value, sec, domain) {
        let d = new Date();
        d.setTime(d.getTime() + (sec * 1000));
        if (domain !== "") {
            domain = "domain=" + domain
        }
        document.cookie = name + "=" + value + ";expires=" + d.toGMTString() + ";path=/;" + domain;
    }
    static reload() {
        fastjs.qualityOutput("main.reload", "fastjs.reload is only recommend for developer test");
        fastjs.setup()
    }
}
class debounce {
    constructor(ms, callback, pid) {
        if (typeof pid !== "number" && pid) {
            fastjs.throwCrashError("Class.debounce.__constructor__", "unknown type given of pid");
        }
        this.pid = !pid ? fastjs.random(10000000, 99999999) : pid;
        if (typeof ms !== "number" && ms) {
            fastjs.throwCrashError("Class.debounce.__constructor__", "unknown type given of ms");
        }
        this.ms = !ms ? 1000 : ms;
        if (!callback) {
            fastjs.qualityOutput("Class.debounce.__constructor__", "Callback is null, it will not do anything when trigger")
        }
        if (typeof callback !== "function" && callback) {
            if (typeof callback === "string") {
                fastjs.qualityOutput("Class.debounce.__constructor__", "We recommend you use function type to give a javascript")
            } else {
                fastjs.throwCrashError("Class.debounce.__constructor__", "unknown type given of callback");
            }
        }
        this.callback = !callback ? () => {
        } : callback;
        this.trigger = false;
        return this;
    }
    trig() {
        if (this.trigger) {
            clearTimeout(this.trigger);
        }
        this.trigger = setTimeout(() => {
            this.trigger = false;
            if (typeof this.callback === "function") {
                this.callback();
            } else {
                eval(this.callback);
            }
        }, this.ms);
        return this;
    }
}
fastjs.setup();