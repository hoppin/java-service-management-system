package com.hoppinzq.video.dao;

import com.hoppinzq.bean.VideoComment;
import com.hoppinzq.bean.VideoEntity;
import com.hoppinzq.bean.VideoExtra;
import com.hoppinzq.service.bean.RequestInfo;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface VideoDao {

    @Insert("<script>" +
            "insert into video" +
            "<trim prefix='(' suffix=')' suffixOverrides=','>" +
            "<if test=\"video_name_change != null and video_name_change != ''\">video_name_change,</if>" +
            "<if test=\"video_name != null and video_name != ''\">video_name,</if>" +
            "<if test=\"video_path_360P != null and video_path_360P != ''\">video_path_360P,</if>" +
            "<if test=\"video_path_480P != null and video_path_480P != ''\">video_path_480P,</if>" +
            "<if test=\"video_path_720P != null and video_path_720P != ''\">video_path_720P,</if>" +
            "<if test=\"video_path_1080P != null and video_path_1080P != ''\">video_path_1080P,</if>" +
            "<if test=\"video_danmu_path != null and video_danmu_path != ''\">video_danmu_path,</if>" +
            "<if test=\"video_bofang != null and video_bofang != ''\">video_bofang,</if>" +
            "<if test=\"video_xihuan != null and video_xihuan != ''\">video_xihuan,</if>" +
            "<if test=\"video_fenlei_id != null and video_fenlei_id != ''\">video_fenlei_id,</if>" +
            "<if test=\"video_dianzan != null and video_dianzan != ''\">video_dianzan,</if>" +
            "<if test=\"video_miaoshu != null and video_miaoshu != ''\">video_miaoshu,</if>" +
            "<if test=\"video_createTime != null and video_createTime != ''\">video_createTime,</if>" +
            "<if test=\"video_param != null and video_param != ''\">video_param,</if>" +
            "<if test=\"video_time != null and video_time != ''\">video_time,</if>" +
            "<if test=\"video_biaoqian != null and video_biaoqian != ''\">video_biaoqian,</if>" +
            "<if test=\"video_people != null and video_people != ''\">video_people,</if>" +
            "<if test=\"video_image_id != null and video_image_id != ''\">video_image_id,</if>" +
            "<if test=\"video_size_360P != null and video_size_360P != ''\">video_size_360P,</if>" +
            "<if test=\"video_size_480P != null and video_size_480P != ''\">video_size_480P,</if>" +
            "<if test=\"video_size_720P != null and video_size_720P != ''\">video_size_720P,</if>" +
            "<if test=\"video_size_1080P != null and video_size_1080P != ''\">video_size_1080P,</if>" +
            "<if test=\"video_type != null and video_type != ''\">video_type,</if>" +
            "</trim>" +
            "<trim prefix='values (' suffix=')' suffixOverrides=','>" +
            "   <if test=\"video_name_change != null and video_name_change != ''\">#{video_name_change},</if>" +
            "   <if test=\"video_name != null and video_name != ''\">#{video_name},</if>" +
            "   <if test=\"video_path_360P != null and video_path_360P != ''\">#{video_path_360P},</if>" +
            "   <if test=\"video_path_480P != null and video_path_480P != ''\">#{video_path_480P},</if>" +
            "   <if test=\"video_path_720P != null and video_path_720P != ''\">#{video_path_720P},</if>" +
            "   <if test=\"video_path_1080P != null and video_path_1080P != ''\">#{video_path_1080P},</if>" +
            "   <if test=\"video_danmu_path != null and video_danmu_path != ''\">#{video_danmu_path},</if>" +
            "   <if test=\"video_bofang != null and video_bofang != ''\">#{video_bofang},</if>" +
            "   <if test=\"video_xihuan != null and video_xihuan != ''\">#{video_xihuan},</if>" +
            "   <if test=\"video_fenlei_id != null and video_fenlei_id != ''\">#{video_fenlei_id},</if>" +
            "   <if test=\"video_dianzan != null and video_dianzan != ''\">#{video_dianzan},</if>" +
            "   <if test=\"video_miaoshu != null and video_miaoshu != ''\">#{video_miaoshu},</if>" +
            "   <if test=\"video_createTime != null and video_createTime != ''\">#{video_createTime},</if>" +
            "   <if test=\"video_param != null and video_param != ''\">#{video_param},</if>" +
            "   <if test=\"video_time != null and video_time != ''\">#{video_time},</if>" +
            "   <if test=\"video_biaoqian != null and video_biaoqian != ''\">#{video_biaoqian},</if>" +
            "   <if test=\"video_people != null and video_people != ''\">#{video_people},</if>" +
            "   <if test=\"video_image_id != null and video_image_id != ''\">#{video_image_id},</if>" +
            "   <if test=\"video_size_360P != null and video_size_360P != ''\">#{video_size_360P},</if>" +
            "   <if test=\"video_size_480P != null and video_size_480P != ''\">#{video_size_480P},</if>" +
            "   <if test=\"video_size_720P != null and video_size_720P != ''\">#{video_size_720P},</if>" +
            "   <if test=\"video_size_1080P != null and video_size_1080P != ''\">#{video_size_1080P},</if>" +
            "   <if test=\"video_type != null and video_type != ''\">#{video_type},</if>" +
            "</trim>" +
            "</script>")
    @Options(useGeneratedKeys = true, keyProperty = "video_id", keyColumn = "video_id")
    void insertVideo(VideoEntity entity);
    @Select("SELECT v.video_param, v.video_id, v.video_name_change, v.video_name,v.video_miaoshu,\n" +
            "        v.video_path_360P, v.video_path_480P, v.video_path_720P, v.video_path_1080P,\n" +
            "        v.video_danmu_path, v.video_bofang, v.video_xihuan, v.video_fenlei_id, v.video_dianzan,\n" +
            "        v.video_createTime, v.video_time, v.video_image_id, v.video_size_360P, v.video_size_480P,\n" +
            "        v.video_size_720P, v.video_size_1080P, v.video_type ,\n" +
            "        vf.*,m.*,u.userregistername AS 'user_name',u.userhearimage AS 'user_image' FROM video v\n" +
            "        LEFT JOIN video_fenlei vf ON v.video_fenlei_id = vf.video_fenlei_id\n" +
            "        LEFT JOIN image m ON v.video_image_id=m.image_id\n" +
            "        LEFT JOIN t_user u ON v.video_people = u.userid")
    List<Map> getAllVideo();
    List<Map> getVideo(@Param("params") VideoEntity entity);
    Integer countVideo(@Param("params") VideoEntity entity);

    @Select("select * from zhibo_temp")
    List<Map> getAllZhibo();

    @Select("select * from zhibo_temp where zhibo_user_id = #{u_id}")
    Map getZhiboByUser(@Param("u_id") Integer u_id);

    @Select("select zhibo_state from zhibo_temp where zhibo_user_id = #{u_id}")
    Map getZhiboStateByUser(@Param("u_id") Integer u_id);

    @Select("SELECT * FROM video_fenlei")
    List<Map> getVideoFenlei();

    @Select("SELECT * FROM video v " +
            "LEFT JOIN video_fenlei vf ON v.video_fenlei_id = vf.video_fenlei_id " +
            "LEFT JOIN image m ON v.video_image_id=m.image_id " +
            "LEFT JOIN video_extra ve ON v.video_id=ve.video_id " +
            "LEFT JOIN t_user u ON v.video_people = u.userid " +
            "WHERE v.video_id=#{video_id}")
    Map getVideoDetail(@Param("video_id") Integer video_id);

    void updateVideo(VideoEntity entity);

    List<Map> queryVideoBytypeAndNumber(@Param("type") Integer type,
                                        @Param("number") Integer number,
                                        @Param("video_fenlei_id") Integer video_fenlei_id,
                                        @Param("video_name") String video_name
    );

    @Delete("delete from video where video_id=#{video_id}")
    void deleteVideo(Integer video_id);
    @Delete("delete from video_comment where video_id=#{video_id}")
    void deleteVideoCommentByVideoId(Integer video_id);
    @Delete("delete from video_extra where video_id=#{video_id}")
    void deleteVideoExtraByVideoId(Integer video_id);
    @Delete("delete from video_comment where comment_id=#{comment_id}")
    void deleteVideoComment(Integer comment_id);

    @Select("SELECT v.*,m.*,u.userregistername AS 'user_name' FROM video AS v " +
            "LEFT JOIN image AS m ON v.video_image_id=m.image_id " +
            "LEFT JOIN t_user u ON v.video_people = u.userid WHERE v.video_param=1")
    Map getVideoTuijian();

    List<Map> queryComments(@Param("params") Map map);

    @Insert("<script>" +
            "insert into video_comment" +
            "<trim prefix='(' suffix=')' suffixOverrides=','>" +
            "<if test=\"comment_message != null and comment_message != ''\">comment_message,</if>" +
            "<if test=\"comment_people != null and comment_people != 0\">comment_people,</if>" +
            "<if test=\"comment_date != null and comment_date != ''\">comment_date,</if>" +
            "<if test=\"video_id != null \">video_id,</if>" +
            "</trim>" +
            "<trim prefix='values (' suffix=')' suffixOverrides=','>" +
            "   <if test=\"comment_message != null and comment_message != ''\">#{comment_message},</if>" +
            "   <if test=\"comment_people != null and comment_people != 0\">#{comment_people},</if>" +
            "   <if test=\"comment_date != null and comment_date != ''\">#{comment_date},</if>" +
            "   <if test=\"video_id != null\">#{video_id},</if>" +
            "</trim>" +
            "</script>")
    Integer insertComment(VideoComment videoComment);



    @Insert("<script>" +
            "insert into video_extra" +
            "<trim prefix='(' suffix=')' suffixOverrides=','>" +
            "<if test=\"video_prompt_spot != null and video_prompt_spot != ''\">video_prompt_spot,</if>" +
            "<if test=\"video_track != null and video_track != ''\">video_track,</if>" +
            "<if test=\"video_adfront != null and video_adfront != ''\">video_adfront,</if>" +
            "<if test=\"video_adfrontlink != null and video_adfrontlink != ''\">video_adfrontlink,</if>" +
            "<if test=\"video_adpause != null and video_adpause != ''\">video_adpause,</if>" +
            "<if test=\"video_adpauselink != null and video_adpauselink != ''\">video_adpauselink,</if>" +
            "<if test=\"video_previewFile != null and video_previewFile != ''\">video_previewFile,</if>" +
            "<if test=\"video_id != null \">video_id,</if>" +
            "<if test=\"video_adfronttime != null \">video_adfronttime,</if>" +
            "</trim>" +
            "<trim prefix='values (' suffix=')' suffixOverrides=','>" +
            "   <if test=\"video_prompt_spot != null and video_prompt_spot != ''\">#{video_prompt_spot},</if>" +
            "   <if test=\"video_track != null and video_track != ''\">#{video_track},</if>" +
            "   <if test=\"video_adfront != null and video_adfront != ''\">#{video_adfront},</if>" +
            "   <if test=\"video_adfrontlink != null and video_adfrontlink != ''\">#{video_adfrontlink},</if>" +
            "   <if test=\"video_adpause != null and video_adpause != ''\">#{video_adpause},</if>" +
            "   <if test=\"video_adpauselink != null and video_adpauselink != ''\">#{video_adpauselink},</if>" +
            "   <if test=\"video_previewFile != null and video_previewFile != ''\">#{video_previewFile},</if>" +
            "   <if test=\"video_id != null\">#{video_id},</if>" +
            "   <if test=\"video_adfronttime != null\">#{video_adfronttime},</if>" +
            "</trim>" +
            "</script>")
    void insertVideoExtra(VideoExtra videoExtra);

    List<Map> queryVideoByTypeAndNumber(@Param("type") Integer type,
                                        @Param("number") Integer number,
                                        @Param("video_fenlei_id") Integer video_fenlei_id,
                                        @Param("video_name") String video_name
    );

    @Insert("insert into my_log(classMethod,httpMethod,createTime,logLevel,requestParams" +
            ",result,ip,url,exception,timeCost) values(#{log.classMethod}," +
            "#{log.httpMethod},#{log.createTime},#{log.logLevel},#{log.requestParams}," +
            "#{log.result},#{log.ip},#{log.url},#{log.exception},#{log.timeCost})")
    void insertLog(@Param(value = "log") RequestInfo requestInfo);

}
