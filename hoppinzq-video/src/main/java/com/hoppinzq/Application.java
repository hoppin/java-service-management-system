package com.hoppinzq;

import com.hoppinzq.service.aop.annotation.EnableGateway;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@EnableGateway
@SpringBootApplication
@ServletComponentScan
@MapperScan(basePackages = "com.hoppinzq.video.dao")
public class Application {
    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class,args);
    }
}