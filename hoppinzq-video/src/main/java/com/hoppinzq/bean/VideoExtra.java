package com.hoppinzq.bean;


/**
 * @author:ZhangQi
 **/
public class VideoExtra {

    private Integer video_extra_id;
    private Integer video_id;
    private Integer video_adfronttime;

    private String video_prompt_spot;
    private String video_track;
    private String video_adfront;
    private String video_adfrontlink;
    private String video_adpause;
    private String video_adpauselink;
    private String video_previewFile;

    public Integer getVideo_extra_id() {
        return video_extra_id;
    }

    public void setVideo_extra_id(Integer video_extra_id) {
        this.video_extra_id = video_extra_id;
    }

    public Integer getVideo_id() {
        return video_id;
    }

    public void setVideo_id(Integer video_id) {
        this.video_id = video_id;
    }

    public Integer getVideo_adfronttime() {
        return video_adfronttime;
    }

    public void setVideo_adfronttime(Integer video_adfronttime) {
        this.video_adfronttime = video_adfronttime;
    }

    public String getVideo_prompt_spot() {
        return video_prompt_spot;
    }

    public void setVideo_prompt_spot(String video_prompt_spot) {
        this.video_prompt_spot = video_prompt_spot;
    }

    public String getVideo_track() {
        return video_track;
    }

    public void setVideo_track(String video_track) {
        this.video_track = video_track;
    }

    public String getVideo_adfront() {
        return video_adfront;
    }

    public void setVideo_adfront(String video_adfront) {
        this.video_adfront = video_adfront;
    }

    public String getVideo_adfrontlink() {
        return video_adfrontlink;
    }

    public void setVideo_adfrontlink(String video_adfrontlink) {
        this.video_adfrontlink = video_adfrontlink;
    }

    public String getVideo_adpause() {
        return video_adpause;
    }

    public void setVideo_adpause(String video_adpause) {
        this.video_adpause = video_adpause;
    }

    public String getVideo_adpauselink() {
        return video_adpauselink;
    }

    public void setVideo_adpauselink(String video_adpauselink) {
        this.video_adpauselink = video_adpauselink;
    }

    public String getVideo_previewFile() {
        return video_previewFile;
    }

    public void setVideo_previewFile(String video_previewFile) {
        this.video_previewFile = video_previewFile;
    }
}
