package com.hoppinzq.bean;

import java.io.Serializable;

/**
 * @author:ZhangQi
 **/
public class FileEnitiy implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer file_id;
    private String file_name;
    private String file_path;
    private String file_date;
    private Integer file_like;
    private Integer file_download;
    private String file_name_change;
    private String file_type;
    private Integer file_isactive;
    private String file_description;

    public void downloadPlus(){
        this.file_download++;
    }

    public Integer getFile_id() {
        return file_id;
    }

    public void setFile_id(Integer file_id) {
        this.file_id = file_id;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getFile_path() {
        return file_path;
    }

    public void setFile_path(String file_path) {
        this.file_path = file_path;
    }

    public String getFile_date() {
        return file_date;
    }

    public void setFile_date(String file_date) {
        this.file_date = file_date;
    }

    public Integer getFile_like() {
        return file_like;
    }

    public void setFile_like(Integer file_like) {
        this.file_like = file_like;
    }

    public Integer getFile_download() {
        return file_download;
    }

    public void setFile_download(Integer file_download) {
        this.file_download = file_download;
    }

    public String getFile_name_change() {
        return file_name_change;
    }

    public void setFile_name_change(String file_name_change) {
        this.file_name_change = file_name_change;
    }

    public String getFile_type() {
        return file_type;
    }

    public void setFile_type(String file_type) {
        this.file_type = file_type;
    }

    public Integer getFile_isactive() {
        return file_isactive;
    }

    public void setFile_isactive(Integer file_isactive) {
        this.file_isactive = file_isactive;
    }

    public String getFile_description() {
        return file_description;
    }

    public void setFile_description(String file_description) {
        this.file_description = file_description;
    }
}
