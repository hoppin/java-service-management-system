package com.hoppinzq.bean;


import java.io.Serializable;

/**
 * @author:ZhangQi
 **/
public class VideoEntity extends FileEnitiy implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer video_id;
    private String video_name_change;
    private String video_name;
    private String video_path_360P;
    private String video_path_480P;
    private String video_path_720P;
    private String video_path_1080P;
    private String video_danmu_path;
    private Integer video_bofang;
    private Integer video_xihuan;
    private Integer video_fenlei_id;
    private Integer video_dianzan;
    private String video_miaoshu;
    private String video_createTime;
    private Integer video_param;
    private String video_time;
    private String video_people;
    private String video_biaoqian;
    private int video_image_id;
    private Long video_size_360P;
    private Long video_size_480P;
    private Long video_size_720P;
    private Long video_size_1080P;
    private String video_type;

    private Integer video_createTime_extra;
    private Integer video_time_extra;
    private Integer video_p_extra;
    private Integer video_sort_extra;
    private Integer video_page_size;
    private Integer video_page_num;
    private String video_ids;

    public Integer getVideo_id() {
        return video_id;
    }

    public void setVideo_id(Integer video_id) {
        this.video_id = video_id;
    }

    public String getVideo_name_change() {
        return video_name_change;
    }

    public void setVideo_name_change(String video_name_change) {
        this.video_name_change = video_name_change;
    }

    public String getVideo_name() {
        return video_name;
    }

    public void setVideo_name(String video_name) {
        this.video_name = video_name;
    }

    public String getVideo_path_360P() {
        return video_path_360P;
    }

    public void setVideo_path_360P(String video_path_360P) {
        this.video_path_360P = video_path_360P;
    }

    public String getVideo_path_480P() {
        return video_path_480P;
    }

    public void setVideo_path_480P(String video_path_480P) {
        this.video_path_480P = video_path_480P;
    }

    public String getVideo_path_720P() {
        return video_path_720P;
    }

    public void setVideo_path_720P(String video_path_720P) {
        this.video_path_720P = video_path_720P;
    }

    public String getVideo_path_1080P() {
        return video_path_1080P;
    }

    public void setVideo_path_1080P(String video_path_1080P) {
        this.video_path_1080P = video_path_1080P;
    }

    public String getVideo_danmu_path() {
        return video_danmu_path;
    }

    public void setVideo_danmu_path(String video_danmu_path) {
        this.video_danmu_path = video_danmu_path;
    }

    public Integer getVideo_bofang() {
        return video_bofang;
    }

    public void setVideo_bofang(Integer video_bofang) {
        this.video_bofang = video_bofang;
    }

    public Integer getVideo_xihuan() {
        return video_xihuan;
    }

    public void setVideo_xihuan(Integer video_xihuan) {
        this.video_xihuan = video_xihuan;
    }

    public Integer getVideo_fenlei_id() {
        return video_fenlei_id;
    }

    public void setVideo_fenlei_id(Integer video_fenlei_id) {
        this.video_fenlei_id = video_fenlei_id;
    }

    public Integer getVideo_dianzan() {
        return video_dianzan;
    }

    public void setVideo_dianzan(Integer video_dianzan) {
        this.video_dianzan = video_dianzan;
    }

    public String getVideo_miaoshu() {
        return video_miaoshu;
    }

    public void setVideo_miaoshu(String video_miaoshu) {
        this.video_miaoshu = video_miaoshu;
    }

    public String getVideo_createTime() {
        return video_createTime;
    }

    public void setVideo_createTime(String video_createTime) {
        this.video_createTime = video_createTime;
    }

    public Integer getVideo_param() {
        return video_param;
    }

    public void setVideo_param(Integer video_param) {
        this.video_param = video_param;
    }

    public String getVideo_time() {
        return video_time;
    }

    public void setVideo_time(String video_time) {
        this.video_time = video_time;
    }

    public String getVideo_people() {
        return video_people;
    }

    public void setVideo_people(String video_people) {
        this.video_people = video_people;
    }

    public String getVideo_biaoqian() {
        return video_biaoqian;
    }

    public void setVideo_biaoqian(String video_biaoqian) {
        this.video_biaoqian = video_biaoqian;
    }

    public int getVideo_image_id() {
        return video_image_id;
    }

    public void setVideo_image_id(int video_image_id) {
        this.video_image_id = video_image_id;
    }

    public Long getVideo_size_360P() {
        return video_size_360P;
    }

    public void setVideo_size_360P(Long video_size_360P) {
        this.video_size_360P = video_size_360P;
    }

    public Long getVideo_size_480P() {
        return video_size_480P;
    }

    public void setVideo_size_480P(Long video_size_480P) {
        this.video_size_480P = video_size_480P;
    }

    public Long getVideo_size_720P() {
        return video_size_720P;
    }

    public void setVideo_size_720P(Long video_size_720P) {
        this.video_size_720P = video_size_720P;
    }

    public Long getVideo_size_1080P() {
        return video_size_1080P;
    }

    public void setVideo_size_1080P(Long video_size_1080P) {
        this.video_size_1080P = video_size_1080P;
    }

    public String getVideo_type() {
        return video_type;
    }

    public void setVideo_type(String video_type) {
        this.video_type = video_type;
    }

    public Integer getVideo_createTime_extra() {
        return video_createTime_extra;
    }

    public void setVideo_createTime_extra(Integer video_createTime_extra) {
        this.video_createTime_extra = video_createTime_extra;
    }

    public Integer getVideo_time_extra() {
        return video_time_extra;
    }

    public void setVideo_time_extra(Integer video_time_extra) {
        this.video_time_extra = video_time_extra;
    }

    public Integer getVideo_p_extra() {
        return video_p_extra;
    }

    public void setVideo_p_extra(Integer video_p_extra) {
        this.video_p_extra = video_p_extra;
    }

    public Integer getVideo_sort_extra() {
        return video_sort_extra;
    }

    public void setVideo_sort_extra(Integer video_sort_extra) {
        this.video_sort_extra = video_sort_extra;
    }

    public Integer getVideo_page_size() {
        return video_page_size;
    }

    public void setVideo_page_size(Integer video_page_size) {
        this.video_page_size = video_page_size;
    }

    public Integer getVideo_page_num() {
        return video_page_num;
    }

    public void setVideo_page_num(Integer video_page_num) {
        this.video_page_num = video_page_num;
    }

    public String getVideo_ids() {
        return video_ids;
    }

    public void setVideo_ids(String video_ids) {
        this.video_ids = video_ids;
    }
}
