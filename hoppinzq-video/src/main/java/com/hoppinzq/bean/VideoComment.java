package com.hoppinzq.bean;


/**
 * @author:ZhangQi
 **/
public class VideoComment {

    private int comment_id;
    private String comment_message;
    private String comment_date;
    private int comment_people;
    private int video_id;

    public int getComment_id() {
        return comment_id;
    }

    public void setComment_id(int comment_id) {
        this.comment_id = comment_id;
    }

    public String getComment_message() {
        return comment_message;
    }

    public void setComment_message(String comment_message) {
        this.comment_message = comment_message;
    }

    public String getComment_date() {
        return comment_date;
    }

    public void setComment_date(String comment_date) {
        this.comment_date = comment_date;
    }

    public int getComment_people() {
        return comment_people;
    }

    public void setComment_people(int comment_people) {
        this.comment_people = comment_people;
    }

    public int getVideo_id() {
        return video_id;
    }

    public void setVideo_id(int video_id) {
        this.video_id = video_id;
    }
}
