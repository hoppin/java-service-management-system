package com.hoppinzq.runner;

import com.hoppinzq.video.service.VideoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(1)
public class CacheRunner implements ApplicationRunner {
    private static final Logger logger = LoggerFactory.getLogger(VideoService.class);

    @Autowired
    private VideoService videoService;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        logger.debug("开始预热缓存");
        //类被入redis
        videoService.getAllVideo2Redis(0);
        logger.debug("预热缓存结束");
    }
}