package com.hoppinzq.service.controller;

import com.alibaba.fastjson.JSONObject;
import com.hoppinzq.service.ServiceProxyFactory;
import com.hoppinzq.service.bean.ApiResponse;
import com.hoppinzq.service.bean.RPCPropertyBean;
import com.hoppinzq.service.bean.ServiceApiBean;
import com.hoppinzq.service.bean.User;
import com.hoppinzq.service.cache.apiCache;
import com.hoppinzq.service.common.UserPrincipal;
import com.hoppinzq.service.file.FileService;
import com.hoppinzq.service.interfaceService.LoginService;
import com.hoppinzq.service.jar.JarService;
import com.hoppinzq.service.util.CookieUtils;
import com.hoppinzq.service.util.RedisUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.List;

/**
 * markdown图片上传controller，因为markdown编辑器上传文件要求固定的返回值
 */
@RestController
@RequestMapping("/jar")
public class JarFileUploadController {
    @Autowired
    private JarService jarService;
    @Autowired
    private FileService fileService;

    @PostMapping(value = "/file/fileUpload")
    public ApiResponse jarupload(MultipartFile file, String projectName, String ms,String version, int port,String userno,Boolean isJMX,Boolean isDebugger){
        try{
            jarService.jarFile(file,projectName,ms,version,port,userno,isJMX,isDebugger);
            return ApiResponse.success("上传成功");
        }catch (Exception ex){
            ex.printStackTrace();
            return ApiResponse.fail(500,"上传失败!"+ex.getMessage());
        }
    }

    /**
     * 一个一个文件上传，本来是想询问是否覆盖的
     * @param file
     * @param dir
     * @return
     */
    @PostMapping(value = "/file/fileUpload3")
    public ApiResponse fileupload(@RequestParam("file") MultipartFile file,String dir){
        try{
            fileService.uploadfile(file,dir);
            return ApiResponse.success("上传成功");
        }catch (Exception ex){
            ex.printStackTrace();
            return ApiResponse.fail(500,"上传失败!"+ex.getMessage());
        }
    }

    @PostMapping(value = "/file/fileUpload2")
    public ApiResponse fileupload(@RequestParam("files") MultipartFile[] files,String dir){
        try{
            //多文件上传预留
            return ApiResponse.success("上传成功");
        }catch (Exception ex){
            ex.printStackTrace();
            return ApiResponse.fail(500,"上传失败!"+ex.getMessage());
        }
    }

    @GetMapping(value = "/file/fileDownload/{id}")
    public void downloadJar(@PathVariable("id")String id, HttpServletResponse response) throws Exception {
        jarService.downloadJar(id,response);
    }

    @GetMapping(value = "/file/exportHead/{id}")
    public void exportHead(@PathVariable("id")String id, HttpServletResponse response) throws Exception {
        jarService.exportHead(id,response);
    }
    @GetMapping(value = "/file/downloadFile")
    public void downloadFile(String fileName, HttpServletResponse response) throws Exception {
        fileService.downloadFile(fileName,response);
    }


    private static File convert(MultipartFile multipartFile) throws IOException {
        File file = new File(multipartFile.getOriginalFilename());
        FileOutputStream outputStream = new FileOutputStream(file);
        outputStream.write(multipartFile.getBytes());
        outputStream.close();
        return file;
    }

}
