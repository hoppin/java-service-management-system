package com.hoppinzq.service.controller;

import com.alibaba.fastjson.JSONObject;
import com.hoppinzq.service.chatgpt.ChatGPTService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestController
@RequestMapping("/chatgpt")
public class GptController {

    @Autowired
    private ChatGPTService chatGPTService;

    @RequestMapping("v1/{param1}/{param2}")
    public String gptApi(@PathVariable("param1") String param1, @PathVariable("param2") String param2, @RequestBody Map querys , HttpServletRequest request){
        String token=request.getHeader("Authorization");
        String apiUrl="v1/"+param1+"/"+param2;
        JSONObject jsonObject=new JSONObject(querys);
        return chatGPTService.gptAPI(apiUrl,token,jsonObject.toJSONString(),true);
    }

    @RequestMapping("notProxy/v1/{param1}/{param2}")
    public String gptApi2(@PathVariable("param1") String param1, @PathVariable("param2") String param2, @RequestBody Map querys , HttpServletRequest request){
        String token=request.getHeader("Authorization");
        String apiUrl="v1/"+param1+"/"+param2;
        JSONObject jsonObject=new JSONObject(querys);
        return chatGPTService.gptAPI(apiUrl,token,jsonObject.toJSONString(),false);
    }
}
