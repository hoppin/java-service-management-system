package com.hoppinzq.service.cache;

import com.alibaba.fastjson.JSONObject;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;

import java.util.concurrent.TimeUnit;

public class SSHConnectCache {
    public static Cache<String, JSONObject> cache;
    static {
        CacheBuilder<Object, Object> builder = CacheBuilder.newBuilder()
                .maximumSize(100) // 缓存最大容量
                .expireAfterWrite(60, TimeUnit.MINUTES); // 写入后 60 分钟过期
        cache = builder.build();
    }
}
