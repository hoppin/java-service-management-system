package com.hoppinzq.service.jar;

import javax.websocket.Session;

/**
 * @author: ZhangQi
 */
public class SessionContext {
    public static ThreadLocal<Session> sessionHold = new ThreadLocal<Session>();

    public static final Session getPrincipal() {
        return sessionHold.get();
    }
    public static final void setRequestHold(Session session) {
        sessionHold.set(session);
    }

    public static void exit() {
        sessionHold.set(null);
    }

    public static void enter(Session principal) {
        sessionHold.set(principal);
    }
}
