package com.hoppinzq.service.http;

import java.nio.charset.StandardCharsets;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;
 
public class MurmurHashTest {
 
    public static void main(String[] args) {
        for (int i = 0; i < 100; i++) {
            String hexHashString = getHexHashString("http://hoppin.cn/hoppinzq?method=mainBlog&params={}");
            System.out.println(hexHashString);
        }
    }
 
    public static String getHexHashString(String str) {
        HashFunction hashFunction = Hashing.murmur3_128();
        return hashFunction.hashString(str, StandardCharsets.UTF_8).toString();
    }
}
