package com.hoppinzq.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hoppinzq.service.bean.JarServiceBean;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface JarServiceMapper extends BaseMapper<JarServiceBean> {
}