package com.hoppinzq.service.dao;

import com.hoppinzq.service.bean.HttpRequestBean;
import org.apache.ibatis.annotations.*;

@Mapper
public interface ManagerDao {
    @Insert("insert into httprequest(request_url,proxy_url,request_type,user_id) values(#{requestBean.request_url}," +
            "#{requestBean.proxy_url},#{requestBean.request_type},#{requestBean.user_id})")
    void insertRequest(@Param(value = "requestBean") HttpRequestBean requestBean);
    HttpRequestBean getRequestUrl(String proxy_url);
}
