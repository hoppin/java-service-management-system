package com.hoppinzq.service.aop;

import com.hoppinzq.service.bean.RequestInfo;
import com.hoppinzq.service.dao.LogDao;
import com.hoppinzq.service.util.DateFormatUtil;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

/**
 * @author:ZhangQi
 **/
@Component
@Aspect
public class ControllerLog {
    private final static Logger LOGGER = LoggerFactory.getLogger(ControllerLog.class);
    @Resource
    private LogDao logDao;

    @Pointcut("execution(* com.hoppinzq.service.controller..*(..))")
    public void requestServer() {
    }

    @Around("requestServer()")
    @Transactional(readOnly = false)
    public Object doAround(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
        long start = System.currentTimeMillis();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        Object result = proceedingJoinPoint.proceed();
        RequestInfo requestInfo = new RequestInfo();
        requestInfo.setIp(request.getRemoteAddr());
        requestInfo.setUrl(request.getRequestURL().toString());
        requestInfo.setHttpMethod(request.getMethod());
        requestInfo.setClassMethod(String.format("%s.%s", proceedingJoinPoint.getSignature().getDeclaringTypeName(),
                proceedingJoinPoint.getSignature().getName()));
        requestInfo.setRequestParams(getRequestParamsByProceedingJoinPoint(proceedingJoinPoint).toString());
        requestInfo.setResult(result==null?"return void":result.toString());
        long createTime=System.currentTimeMillis();
        requestInfo.setTimeCost(createTime - start);
        System.out.println("接口:"+request.getRequestURL().toString()+" 请求时长："+(createTime - start));
        requestInfo.setCreateTime(DateFormatUtil.stampToDate(createTime));
        requestInfo.setLogLevel("INFO");
        if(!request.getRemoteAddr().equals("127.0.0.1")){
            logDao.insertLog(requestInfo);
        }
        System.out.println("----------------开始-----------------------");
        LOGGER.info("Request Info:\n {}", requestInfo.toString());
        System.out.println("----------------结束-----------------------");

        return result;
    }


    @AfterThrowing(pointcut = "requestServer()", throwing = "e")
    @Transactional(readOnly = false)
    public void doAfterThrow(JoinPoint joinPoint, RuntimeException e) {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        RequestInfo requestErrorInfo = new RequestInfo();
        requestErrorInfo.setIp(request.getRemoteAddr());
        requestErrorInfo.setUrl(request.getRequestURL().toString());
        requestErrorInfo.setHttpMethod(request.getMethod());
        requestErrorInfo.setClassMethod(String.format("%s.%s", joinPoint.getSignature().getDeclaringTypeName(),
                joinPoint.getSignature().getName()));
        requestErrorInfo.setRequestParams(getRequestParamsByJoinPoint(joinPoint).toString());
        requestErrorInfo.setException(e.getMessage());
        long createTime=System.currentTimeMillis();
        requestErrorInfo.setCreateTime(DateFormatUtil.stampToDate(createTime));
        requestErrorInfo.setLogLevel("Error");
        if(!request.getRemoteAddr().equals("127.0.0.1")){
            logDao.insertLog(requestErrorInfo);
        }
        System.out.println("----------------开始-----------------------");
        LOGGER.error("Error Request Info:\n {}", requestErrorInfo.toString());
        System.out.println("----------------结束-----------------------");
    }

    /**
     * 获取入参
     * @param proceedingJoinPoint
     *
     * @return
     * */
    private Map<String, Object> getRequestParamsByProceedingJoinPoint(ProceedingJoinPoint proceedingJoinPoint) {
        //参数名
        String[] paramNames = ((MethodSignature)proceedingJoinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = proceedingJoinPoint.getArgs();

        return buildRequestParam(paramNames, paramValues);
    }

    private Map<String, Object> getRequestParamsByJoinPoint(JoinPoint joinPoint) {
        //参数名
        String[] paramNames = ((MethodSignature)joinPoint.getSignature()).getParameterNames();
        //参数值
        Object[] paramValues = joinPoint.getArgs();

        return buildRequestParam(paramNames, paramValues);
    }

    private Map<String, Object> buildRequestParam(String[] paramNames, Object[] paramValues) {
        Map<String, Object> requestParams = new HashMap<>();
        for (int i = 0; i < paramNames.length; i++) {
            Object value = paramValues[i];

            //如果是文件对象
            if (value instanceof MultipartFile) {
                MultipartFile file = (MultipartFile) value;
                value = file.getOriginalFilename();  //获取文件名
            }

            requestParams.put(paramNames[i], value);
        }

        return requestParams;
    }

}
