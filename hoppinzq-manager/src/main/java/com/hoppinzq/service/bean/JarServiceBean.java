package com.hoppinzq.service.bean;

import com.baomidou.mybatisplus.annotation.*;

import java.util.Date;

@TableName("jarservice")
public class JarServiceBean {

    @TableId(type = IdType.INPUT)
    private String id;
    @TableField("jar_name")
    private String jar_name;
    @TableField("service_name")
    private String service_name;
    @TableField("service_de")
    private String service_de;
    @TableField("create_time")
    private Date create_time;
    @TableField("version")
    private String version;
    @TableField("pid")
    private int pid;
    @TableField("state")
    private int state;
    @TableField("jar_path")
    private String jar_path;
    @TableField("jar_dir")
    private String jar_dir;
    @TableField("jar_sh_path")
    private String jar_sh_path;
    @TableField("jar_rezhi_path")
    private String jar_rezhi_path;
    @TableField("port")
    private int port;
    @TableField("user")
    private String user;
    @TableField("read_state")
    private int read_state;//只读状态，0表示只读，不允许操作
    @TableField("jmx_port")
    private int jmxPort;
    @TableField("debugger_port")
    private int debuggerPort;

    public int getJmxPort() {
        return jmxPort;
    }

    public void setJmxPort(int jmxPort) {
        this.jmxPort = jmxPort;
    }


    public int getDebuggerPort() {
        return debuggerPort;
    }

    public void setDebuggerPort(int debuggerPort) {
        this.debuggerPort = debuggerPort;
    }

    public int getRead_state() {
        return read_state;
    }

    public void setRead_state(int read_state) {
        this.read_state = read_state;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getJar_name() {
        return jar_name;
    }

    public void setJar_name(String jar_name) {
        this.jar_name = jar_name;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getService_de() {
        return service_de;
    }

    public void setService_de(String service_de) {
        this.service_de = service_de;
    }

    public Date getCreate_time() {
        return create_time;
    }

    public void setCreate_time(Date create_time) {
        this.create_time = create_time;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        if(version==null||"".equals(version))
            version="V1.0";
        this.version = version;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getJar_path() {
        return jar_path;
    }

    public void setJar_path(String jar_path) {
        this.jar_path = jar_path;
    }

    public String getJar_dir() {
        return jar_dir;
    }

    public void setJar_dir(String jar_dir) {
        this.jar_dir = jar_dir;
    }

    public String getJar_sh_path() {
        return jar_sh_path;
    }

    public void setJar_sh_path(String jar_sh_path) {
        this.jar_sh_path = jar_sh_path;
    }

    public String getJar_rezhi_path() {
        return jar_rezhi_path;
    }

    public void setJar_rezhi_path(String jar_rezhi_path) {
        this.jar_rezhi_path = jar_rezhi_path;
    }
}
