/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.18-cynos-log : Database - video
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`video` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `video`;

/*Table structure for table `blog` */

DROP TABLE IF EXISTS `blog`;

CREATE TABLE `blog` (
  `blog_id` int(5) NOT NULL AUTO_INCREMENT,
  `blog_title` varchar(32) DEFAULT NULL,
  `blog_author` varchar(18) DEFAULT NULL,
  `blog_date` date DEFAULT NULL,
  `blog_readtime` int(5) DEFAULT NULL,
  `blog_commenttime` int(5) DEFAULT NULL,
  `blog_showmessage` varchar(128) DEFAULT NULL,
  `blog_message` longtext,
  `blog_type` varchar(38) DEFAULT NULL,
  `blog_image` varchar(256) DEFAULT NULL,
  `blog_isme` int(11) DEFAULT NULL,
  `blog_like` int(5) DEFAULT NULL,
  `blog_file_id` int(5) DEFAULT NULL COMMENT '附件',
  PRIMARY KEY (`blog_id`)
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;

/*Table structure for table `blog_type` */

DROP TABLE IF EXISTS `blog_type`;

CREATE TABLE `blog_type` (
  `blog_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `blog_type_name` varchar(18) DEFAULT NULL,
  `blog_type_screen` varchar(18) DEFAULT NULL,
  PRIMARY KEY (`blog_type_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

/*Table structure for table `english` */

DROP TABLE IF EXISTS `english`;

CREATE TABLE `english` (
  `englishid` int(11) NOT NULL AUTO_INCREMENT,
  `english` varchar(30) NOT NULL,
  `englishc` varchar(500) NOT NULL COMMENT '中文',
  `englishj` varchar(2000) DEFAULT NULL COMMENT '句子',
  `englishjc` varchar(500) DEFAULT NULL COMMENT '句子翻译',
  `englishcz` varchar(500) DEFAULT NULL COMMENT '词组',
  `englishczc` varchar(100) DEFAULT NULL COMMENT '词组翻译',
  `englishlevel` int(11) DEFAULT '1' COMMENT '英语等级12345',
  `englishday` int(11) DEFAULT NULL COMMENT '英语日期',
  `englishinszb` int(11) DEFAULT '0' COMMENT '收藏了？01',
  `englishdate` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '收录日期',
  PRIMARY KEY (`englishid`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `feedback_id` int(8) NOT NULL AUTO_INCREMENT,
  `feedback_name` varchar(18) DEFAULT NULL,
  `feedback_callme` varchar(28) DEFAULT NULL,
  `feedback_message` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`feedback_id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `file_id` int(5) NOT NULL AUTO_INCREMENT,
  `file_name` varchar(128) DEFAULT NULL,
  `file_path` varchar(128) DEFAULT NULL,
  `file_date` varchar(128) DEFAULT NULL,
  `file_like` int(5) DEFAULT NULL,
  `file_download` int(5) DEFAULT '0',
  `file_name_change` varchar(128) DEFAULT NULL,
  `file_type` varchar(128) DEFAULT NULL,
  `file_isactive` int(2) DEFAULT '0' COMMENT '0可用1私密',
  `file_description` varchar(512) DEFAULT NULL COMMENT '描述',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=114 DEFAULT CHARSET=utf8;

/*Table structure for table `httprequest` */

DROP TABLE IF EXISTS `httprequest`;

CREATE TABLE `httprequest` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `request_url` varchar(1024) NOT NULL,
  `proxy_url` varchar(1024) DEFAULT NULL,
  `request_type` varchar(16) DEFAULT 'GET',
  `user_id` varchar(64) DEFAULT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `image` */

DROP TABLE IF EXISTS `image`;

CREATE TABLE `image` (
  `image_id` int(8) NOT NULL AUTO_INCREMENT,
  `image_name_change` varchar(64) DEFAULT NULL,
  `image_name` varchar(256) DEFAULT NULL,
  `image_path` varchar(128) DEFAULT NULL,
  `image_date` varchar(64) DEFAULT NULL,
  `image_size` bigint(20) DEFAULT NULL,
  `image_type` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=201 DEFAULT CHARSET=utf8;

/*Table structure for table `jarservice` */

DROP TABLE IF EXISTS `jarservice`;

CREATE TABLE `jarservice` (
  `id` varchar(64) NOT NULL,
  `jar_name` varchar(64) DEFAULT NULL,
  `service_name` varchar(128) DEFAULT NULL,
  `service_de` varchar(512) DEFAULT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `version` varchar(8) DEFAULT NULL,
  `pid` int(11) DEFAULT NULL COMMENT '进程号',
  `state` smallint(1) DEFAULT '0' COMMENT '0停止1运行中',
  `jar_path` varchar(128) DEFAULT NULL COMMENT 'jar包地址',
  `jar_dir` varchar(128) DEFAULT NULL COMMENT '父级地址',
  `jar_sh_path` varchar(128) DEFAULT NULL COMMENT '脚本存放地址',
  `jar_rezhi_path` varchar(128) DEFAULT NULL COMMENT '日志存放地址',
  `port` int(5) DEFAULT NULL COMMENT '端口号',
  `user` varchar(64) DEFAULT NULL,
  `read_state` smallint(1) DEFAULT '1' COMMENT '只读状态',
  `jmx_port` int(11) DEFAULT '0' COMMENT 'jmx端口号，0表示不使用jmx',
  `debugger_port` int(11) DEFAULT '0' COMMENT '远程调试端口号，0表示不使用',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `money` */

DROP TABLE IF EXISTS `money`;

CREATE TABLE `money` (
  `moneyid` int(8) NOT NULL AUTO_INCREMENT,
  `moneytype` int(2) DEFAULT NULL COMMENT '1:表示收入，2：房租支出，3：水电费，4：交通费，5：伙食费，6：日用品费，7：其他',
  `moneynum` double DEFAULT NULL COMMENT '花费数目/收入',
  `moneytime` date DEFAULT NULL COMMENT '何时花费/天',
  `moneybuywhat` varchar(80) DEFAULT NULL COMMENT '买了什么',
  `moneytext` varchar(800) DEFAULT NULL COMMENT '买物品备注',
  `moneypay` int(1) DEFAULT NULL,
  PRIMARY KEY (`moneyid`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

/*Table structure for table `music` */

DROP TABLE IF EXISTS `music`;

CREATE TABLE `music` (
  `music_id` int(8) NOT NULL AUTO_INCREMENT,
  `music_name` varchar(32) DEFAULT NULL,
  `music_address` varchar(128) DEFAULT NULL,
  `music_artist` varchar(32) DEFAULT NULL,
  `wangyiyun_music_id` int(32) DEFAULT NULL,
  PRIMARY KEY (`music_id`)
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=utf8;

/*Table structure for table `my_log` */

DROP TABLE IF EXISTS `my_log`;

CREATE TABLE `my_log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `httpMethod` varchar(128) DEFAULT NULL,
  `classMethod` varchar(128) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `logLevel` varchar(11) DEFAULT NULL,
  `requestParams` longblob,
  `timeCost` int(8) DEFAULT NULL,
  `result` longblob,
  `exception` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29999 DEFAULT CHARSET=utf8;

/*Table structure for table `people` */

DROP TABLE IF EXISTS `people`;

CREATE TABLE `people` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `name` varchar(18) DEFAULT NULL,
  `password` varchar(64) DEFAULT NULL,
  `email` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38 DEFAULT CHARSET=utf8;

/*Table structure for table `t_english` */

DROP TABLE IF EXISTS `t_english`;

CREATE TABLE `t_english` (
  `english_id` int(6) NOT NULL AUTO_INCREMENT COMMENT '英语id',
  `english_word` varchar(64) DEFAULT NULL COMMENT '英语单词',
  `english_word_translate_cn` varchar(64) DEFAULT NULL COMMENT '英语单词中文翻译',
  `english_word_sound_file_id` int(6) DEFAULT NULL COMMENT '英语单词发音文件id',
  `english_word_sound_file_path` varchar(128) DEFAULT NULL COMMENT '英语单词发音文件路径',
  `english_word_part_of_speech` varchar(10) DEFAULT NULL COMMENT '英语单词词性',
  `english_word_part_of_speech_translate_cn` varchar(32) DEFAULT NULL COMMENT '英语单词词性翻译',
  `english_phrase` varchar(256) DEFAULT NULL COMMENT '英语短语，多个用,分割',
  `english_phrase_tranlate_cn` varchar(256) DEFAULT NULL COMMENT '英语短语中文翻译，多个用,分割',
  `english_phrase_sound_file_id` int(6) DEFAULT NULL COMMENT '英语短语发音文件id',
  `english_phrase_sound_file_path` varchar(128) DEFAULT NULL COMMENT '英语短语发音文件路径',
  `english_exempli_sentences` varchar(1024) DEFAULT NULL COMMENT '英语单词例句',
  `english_exempli_sentences_translate` varchar(256) DEFAULT NULL COMMENT '英语单词例句翻译',
  `english_exempli_sentences_sound_file_id` int(6) DEFAULT NULL COMMENT '英语单词例句发音文件id',
  `english_exempli_sentences_sound_file_path` varchar(128) DEFAULT NULL COMMENT '英语单词例句发音文件路径',
  `english_extra_tips` varchar(1024) DEFAULT NULL COMMENT '英语单词的一些拓展解释',
  `english_type` int(2) DEFAULT '0' COMMENT '英语类型：0：无分类，1.四级词汇；2.六级词汇；3.考研词汇；4.其他词汇',
  `english_create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '英语单词创建时间',
  `english_iscollect` int(1) DEFAULT '0' COMMENT '英语单词是否收藏？0.未收藏；1.已收藏',
  `english_level` int(1) DEFAULT '1' COMMENT '英语单词难易打分，1-5从易到难',
  `english_belong_part` int(3) DEFAULT NULL COMMENT '英语单词属于哪个模块',
  `english_function` int(4) DEFAULT '0' COMMENT '英语功能，0.单词功能，1.例句功能......',
  PRIMARY KEY (`english_id`),
  KEY `english_word_sound_file_id` (`english_word_sound_file_id`),
  KEY `english_phrase_sound_file_id` (`english_phrase_sound_file_id`),
  KEY `english_exempli_sentences_sound_file_id` (`english_exempli_sentences_sound_file_id`),
  CONSTRAINT `t_english_ibfk_1` FOREIGN KEY (`english_word_sound_file_id`) REFERENCES `file` (`file_id`),
  CONSTRAINT `t_english_ibfk_2` FOREIGN KEY (`english_phrase_sound_file_id`) REFERENCES `file` (`file_id`),
  CONSTRAINT `t_english_ibfk_3` FOREIGN KEY (`english_exempli_sentences_sound_file_id`) REFERENCES `file` (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `t_user` */

DROP TABLE IF EXISTS `t_user`;

CREATE TABLE `t_user` (
  `userid` int(8) NOT NULL AUTO_INCREMENT,
  `username` varchar(18) DEFAULT NULL,
  `usersex` varchar(2) DEFAULT NULL,
  `userbrith` varchar(50) DEFAULT NULL,
  `userage` int(2) DEFAULT NULL,
  `usermessage` varchar(800) DEFAULT NULL,
  `userqq` varchar(11) DEFAULT NULL,
  `userphone` varchar(22) DEFAULT NULL,
  `userblood` char(4) DEFAULT NULL,
  `userpower` varchar(100) DEFAULT NULL COMMENT '一个暂时写的权限字段',
  `userhearimage` varchar(200) DEFAULT NULL COMMENT '头像',
  `userposition` varchar(100) DEFAULT NULL COMMENT '职位，3个，用，隔开',
  `useraddress` varchar(221) DEFAULT NULL COMMENT '地址',
  `usernature` varchar(100) DEFAULT NULL COMMENT '性格',
  `userstate` varchar(2) DEFAULT NULL,
  `userjmpassword` varchar(128) DEFAULT NULL,
  `userpassword` varchar(18) DEFAULT NULL,
  `userregistername` varchar(18) DEFAULT NULL,
  `useremail` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

/*Table structure for table `talk` */

DROP TABLE IF EXISTS `talk`;

CREATE TABLE `talk` (
  `talk_id` int(11) NOT NULL AUTO_INCREMENT,
  `talk_message_user` varchar(256) DEFAULT NULL,
  `talk_message_bot` varchar(256) DEFAULT NULL,
  `talk_data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `talk_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`talk_id`)
) ENGINE=InnoDB AUTO_INCREMENT=124 DEFAULT CHARSET=utf8;

/*Table structure for table `tb_sku` */

DROP TABLE IF EXISTS `tb_sku`;

CREATE TABLE `tb_sku` (
  `id` varchar(20) NOT NULL COMMENT '商品id',
  `name` varchar(200) NOT NULL COMMENT 'SKU名称',
  `price` int(20) NOT NULL COMMENT '价格（分）',
  `num` int(10) NOT NULL COMMENT '库存数量',
  `image` varchar(200) DEFAULT NULL COMMENT '商品图片',
  `category_name` varchar(200) DEFAULT NULL COMMENT '类目名称',
  `brand_name` varchar(100) DEFAULT NULL COMMENT '品牌名称',
  `spec` varchar(200) DEFAULT NULL COMMENT '规格',
  `sale_num` int(11) DEFAULT '0' COMMENT '销量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品表';

/*Table structure for table `tips` */

DROP TABLE IF EXISTS `tips`;

CREATE TABLE `tips` (
  `tips_id` int(5) NOT NULL AUTO_INCREMENT,
  `tips_title` varchar(32) DEFAULT NULL,
  `tips_user` varchar(32) DEFAULT NULL,
  `tips_message` varchar(2047) DEFAULT NULL,
  `tips_date` date DEFAULT NULL,
  PRIMARY KEY (`tips_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Table structure for table `types` */

DROP TABLE IF EXISTS `types`;

CREATE TABLE `types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `video` */

DROP TABLE IF EXISTS `video`;

CREATE TABLE `video` (
  `video_id` int(8) NOT NULL AUTO_INCREMENT,
  `video_name_change` varchar(64) DEFAULT NULL,
  `video_name` varchar(64) DEFAULT NULL,
  `video_path_360P` varchar(128) DEFAULT NULL,
  `video_path_480P` varchar(128) DEFAULT NULL,
  `video_path_720P` varchar(128) DEFAULT NULL,
  `video_path_1080P` varchar(128) DEFAULT NULL,
  `video_danmu_path` varchar(128) DEFAULT NULL,
  `video_bofang` int(8) DEFAULT '0',
  `video_xihuan` int(8) DEFAULT '0',
  `video_fenlei_id` int(8) DEFAULT '0',
  `video_dianzan` int(3) DEFAULT '0',
  `video_miaoshu` longtext,
  `video_createTime` varchar(64) DEFAULT NULL,
  `video_param` int(5) DEFAULT NULL,
  `video_time` varchar(16) DEFAULT NULL,
  `video_people` varchar(16) DEFAULT '2',
  `video_biaoqian` varchar(256) DEFAULT NULL,
  `video_image_id` int(8) DEFAULT NULL,
  `video_size_360P` bigint(20) DEFAULT NULL,
  `video_size_480P` bigint(20) DEFAULT NULL,
  `video_size_720P` bigint(20) DEFAULT NULL,
  `video_size_1080P` bigint(20) DEFAULT NULL,
  `video_type` varchar(6) DEFAULT NULL,
  PRIMARY KEY (`video_id`),
  KEY `video_fenlei_id` (`video_fenlei_id`),
  CONSTRAINT `video_ibfk_1` FOREIGN KEY (`video_fenlei_id`) REFERENCES `video_fenlei` (`video_fenlei_id`)
) ENGINE=InnoDB AUTO_INCREMENT=171 DEFAULT CHARSET=utf8;

/*Table structure for table `video_comment` */

DROP TABLE IF EXISTS `video_comment`;

CREATE TABLE `video_comment` (
  `comment_id` int(8) NOT NULL AUTO_INCREMENT,
  `comment_message` varchar(2000) DEFAULT NULL,
  `comment_people` int(11) DEFAULT '2',
  `comment_date` varchar(64) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`comment_id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;

/*Table structure for table `video_extra` */

DROP TABLE IF EXISTS `video_extra`;

CREATE TABLE `video_extra` (
  `video_id` int(11) DEFAULT NULL COMMENT '视频ID',
  `video_prompt_spot` varchar(2000) DEFAULT NULL COMMENT '视频点30=一面boss&150=二面boss',
  `video_track` varchar(2000) DEFAULT NULL COMMENT '/video/zh.vtt&中文&1',
  `video_adfront` varchar(500) DEFAULT NULL,
  `video_adfronttime` int(11) DEFAULT NULL,
  `video_adfrontlink` varchar(500) DEFAULT NULL,
  `video_adpause` varchar(500) DEFAULT NULL,
  `video_adpauselink` varchar(500) DEFAULT NULL,
  `video_previewFile` varchar(500) DEFAULT NULL COMMENT 'file1&file2',
  `video_extra_id` int(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`video_extra_id`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=utf8;

/*Table structure for table `video_fenlei` */

DROP TABLE IF EXISTS `video_fenlei`;

CREATE TABLE `video_fenlei` (
  `video_fenlei_id` int(8) NOT NULL AUTO_INCREMENT,
  `video_fenlei_name` varchar(64) DEFAULT NULL,
  `video_fenlei_people` varchar(16) DEFAULT NULL,
  `video_fenlei_createTime` date DEFAULT NULL,
  `video_fenlei_miaoshu` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`video_fenlei_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Table structure for table `zhibo_temp` */

DROP TABLE IF EXISTS `zhibo_temp`;

CREATE TABLE `zhibo_temp` (
  `zhibo_id` int(11) DEFAULT NULL,
  `stream_name` varchar(255) DEFAULT NULL,
  `app_name` varchar(255) DEFAULT NULL,
  `zhibo_state` int(11) DEFAULT '0',
  `zhibo_miaoshu` text,
  `zhibo_user_id` int(11) DEFAULT NULL,
  `zhibo_user_uuid` varchar(64) DEFAULT NULL,
  `zhibo_image` varchar(512) DEFAULT NULL,
  `zhibo_title` varchar(256) DEFAULT NULL,
  `zhibo_loge` varchar(256) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
