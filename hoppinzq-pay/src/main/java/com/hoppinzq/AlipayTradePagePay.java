package com.hoppinzq;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.CertAlipayRequest;
import com.alipay.api.AlipayConfig;
import com.alipay.api.domain.AlipayTradePagePayModel;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.FileItem;

public class AlipayTradePagePay {

    public static void main(String[] args) throws AlipayApiException {
        String privateKey = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQC6g17cwYr/ymcaD2JCcJ54sThThE2EINo4rvljU7y0ThVFT+YNKrg0WHUyuzEQjt3XNDY4FJYUQ2lQ5WTCtu5judFp5NghI2mpRIVxPfnCDGcNeUVgFqVsek2F1KpdIyrh/hw3uO1aJ0R29DvUl95AJYDzjREUC9OpxdbxEk/VX2ROn3xwYFwO+tNfRhMSKONT1h4pBwaaaJvAmhx1wK1H/8o1GtplKMJ923TkYTMsvoYPTiPljYcJ1MS3buW4O7pgiVmbvGltGfj9kpHsGIdokCKXiLYoYBLcczdMynbQLjbzvujycizuG7YpWha99F/hPB9oys+afVEusMJLbUrxAgMBAAECggEACrntZbK3xLgmL8h8tSd25agDfkIYlyWwhxwA+aZiLthivsa2i0FOsKv5qX48dyBQP8Uf7R+59dZeoyleHxiYIyIXmghwKY+4zuRC7F1NFMzTzGGSDNGgl9l8xdqSJaMVH0QW6Vl4RPwHIvQu0K4fOP8vKFKPjeCUPqpDD0AnWTT3AxA1ZT7KlAvX+9bGBBf8YN4+qiTglIYwgeNvgRJGo7Qmut8RTPAdhN8KTuM9WDtORYTaAzlMN1gN54YNSEzZnMP0EmXG5k0/lgAW4/l9a/cP7EHoOJqsZJDABLqud4jDMN/uUM5L/sVVt4b4gAS+b/74j1HFqcI8PmwPP5sTxQKBgQDrSguTHvern5aFOitgq/rH1qcZAVtaLayU2jaIsvIGXM0sNJawmz0fltUfeWbrBZJqxroEa0jiysCB1nLxtVA/su22ilo3bWe8p4GBcOn7JdinGxahJSidYhGrufu1Kmo8S0iWodx+jlfjsPEjBGVpJR+omZ0ehLWkDlwQ2CtUbwKBgQDK7jdeqETn8FkYk1vpj64+wC5OAmmJJUuuUt14sYB+KqbNnTovFhntr6FAtNcVCxN8BGBDEFsYFCI8hdXLO+XY+xGyh8hmBanSKAmISsuOz44gCmpOcLcEgbT7eiwRk+LnDZcu4InxrkLN3GnD6Hnx3K+usJc+mX0Y3+tvhSrGnwKBgFfgbtRC46Ow1gnqQNs5QE+05vHa5OCYYHWzzlxd4TDpJiQVfBZ31Gtg7TZ1JDAxA49nq4ANBcrWiPG1Q3btDgzMaiYcJDfBX9ZBBYhvwKPxbapz3o7PDw1A3HKz4sO2FLlyMKk5yN1nkoHSyy0V7j5IBDv3K4yDakfcqRwNcJl3AoGAemLi5+ap8AwnX8f3pcvutY+oHYoc5galNxRo15KNwQmuIdGSNrWom43RvbmTAfD1JlVpTOj/V21FHKVGxFRykcMR8E8t0EHYIpPc8+LQE2gf+jjt1jLqXgBwCz6qd0tujBTJOg9XgnereIZXDz0U1oZXOg7smGdvZ+IcFt0EaFcCgYAifOxIWtcOTOnmEn/kb5ong3C1gFcXBn4wSy2b3Pz6Q9Yf12511IGFaCwZwwAS09bGVXqiqxsJwbGbmBxCb/085p4rQot7VijqoL5cJJ1piVkJKlhBvWD6b21NtMKN0ag6iLZfVryXLM5CN25n5B+yxQyHLYTVPv47+ExCAbHoRA==";
        String alipayPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAiIeCaiLImYC+Auzj1dYB7ItOJp+ap/UaEHi6M9X4+Z2EHdAxj9Z6oSr7YyLliHCLTLNtV+wq/3BemkY44fXR6ezdJqW5FSeNnhNiqZS79tGc9GLRsX1qIVVVmFvDnOL4uTOtg1PWPRJpp+Hwk4QL5vtX84zMrgvK96zRIaJjLc4utoPL7rib5blzHWJE5Pt4m/IpFRI/eelRLyH9LWZyw39VhXIL4A0ZX1sTzzmdqs1LekHkJos/dDMzJ90T2V3LtnABSCsXTr6KmUr3qCkAWlsUq0lvbnY40RkSS3M1H0pBbe+35X7u91p9er6H1P4wzOI735yIRovBRGv1kAhjjwIDAQAB";
        AlipayConfig alipayConfig = new AlipayConfig();
        alipayConfig.setServerUrl("https://openapi.alipaydev.com/gateway.do");
        alipayConfig.setAppId("2016103000780144");
        alipayConfig.setPrivateKey(privateKey);
        alipayConfig.setFormat("json");
        alipayConfig.setAlipayPublicKey(alipayPublicKey);
        alipayConfig.setCharset("UTF8");
        alipayConfig.setSignType("RSA2");
        AlipayClient alipayClient = new DefaultAlipayClient(alipayConfig);
        AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
        AlipayTradePagePayModel model = new AlipayTradePagePayModel();
        model.setOutTradeNo("20150320010101001");
        model.setTotalAmount("88.88");
        model.setSubject("Iphone6 16G");
        model.setProductCode("FAST_INSTANT_TRADE_PAY");
        request.setBizModel(model);
        AlipayTradePagePayResponse response = alipayClient.pageExecute(request);
        System.out.println(response.getBody());
        if (response.isSuccess()) {
            System.out.println("调用成功");
        } else {
            System.out.println("调用失败");
        }
    }
}