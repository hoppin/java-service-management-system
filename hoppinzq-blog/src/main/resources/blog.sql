/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.18-cynos-log : Database - blog
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`blog` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `blog`;

/*Table structure for table `backup` */

DROP TABLE IF EXISTS `backup`;

CREATE TABLE `backup` (
  `id` varchar(42) NOT NULL COMMENT '备份ID',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '备份时间',
  `obj_id` varchar(42) DEFAULT NULL COMMENT '业务ID',
  `text` longtext COMMENT '备份的数据',
  `version` int(5) DEFAULT '1' COMMENT '业务ID的版本号(乐观锁)',
  `message` varchar(1024) DEFAULT NULL COMMENT '备份信息',
  `table` varchar(128) DEFAULT NULL COMMENT '备份的表名',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `blog` */

DROP TABLE IF EXISTS `blog`;

CREATE TABLE `blog` (
  `id` bigint(42) NOT NULL COMMENT '博客id',
  `title` varchar(100) CHARACTER SET utf8 DEFAULT NULL COMMENT '博客标题',
  `description` varchar(256) CHARACTER SET utf8 DEFAULT NULL COMMENT '博客描述',
  `build_type` smallint(1) DEFAULT '1' COMMENT '0简单富文本，1富文本，2markdown，3csdn',
  `csdn_link` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'csdn链接',
  `text` longtext CHARACTER SET utf8 COMMENT '博客内容',
  `blog_like` int(7) DEFAULT '0' COMMENT '喜欢数',
  `star` int(5) DEFAULT '0' COMMENT '评分',
  `collect` int(7) DEFAULT '0' COMMENT '收藏数',
  `author` bigint(42) DEFAULT '1' COMMENT '作者ID',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后一次修改时间',
  `file_path` varchar(1024) CHARACTER SET utf8 DEFAULT NULL COMMENT '附件路径',
  `is_comment` smallint(1) DEFAULT '0' COMMENT '0允许评论，1不允许评论',
  `blog_class` varchar(1024) CHARACTER SET utf8 DEFAULT NULL COMMENT '分类，格式 大类ID||小类ID1|小类ID2|小类ID3',
  `is_create_self` smallint(1) DEFAULT '0' COMMENT '0原创，1转载',
  `music_file` varchar(42) CHARACTER SET utf8 DEFAULT NULL COMMENT '背景音乐id',
  `image` varchar(1024) CHARACTER SET utf8 DEFAULT NULL COMMENT '封面图片，格式 图片1||图片2||...',
  `html` longtext CHARACTER SET utf8 COMMENT '博客内容html',
  `copy_link` varchar(1024) CHARACTER SET utf8 DEFAULT NULL COMMENT '转载链接',
  `type` smallint(1) DEFAULT '0' COMMENT '1草稿，0已完成的博客，2受限制的博客',
  `blog_class_name` varchar(1024) CHARACTER SET utf8 DEFAULT NULL COMMENT '分类，格式 大类名称||小类名称|小类名称|小类名称',
  `file_id` varchar(42) CHARACTER SET utf8 DEFAULT NULL COMMENT '附件ID',
  `author_name` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '作者名字',
  `show_num` int(6) DEFAULT '0' COMMENT '访问量',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

/*Table structure for table `blog_class` */

DROP TABLE IF EXISTS `blog_class`;

CREATE TABLE `blog_class` (
  `id` bigint(42) NOT NULL COMMENT '类别ID',
  `parent_id` bigint(42) DEFAULT NULL COMMENT '父类别ID',
  `name` varchar(64) DEFAULT NULL COMMENT '类别名称',
  `description` varchar(256) DEFAULT NULL COMMENT '类别描述',
  `nickname` varchar(128) DEFAULT NULL COMMENT '类别昵称',
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后一次更新时间',
  `author` varchar(42) DEFAULT '1' COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `blog_class_mid` */

DROP TABLE IF EXISTS `blog_class_mid`;

CREATE TABLE `blog_class_mid` (
  `blog_id` bigint(42) NOT NULL COMMENT '博客ID',
  `class_id` bigint(42) NOT NULL COMMENT '类别ID',
  `auth_id` bigint(42) NOT NULL COMMENT '作者ID',
  KEY `blog_id` (`blog_id`),
  KEY `class_id` (`class_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `blog_cz` */

DROP TABLE IF EXISTS `blog_cz`;

CREATE TABLE `blog_cz` (
  `id` int(6) NOT NULL AUTO_INCREMENT COMMENT '操作ID',
  `set_type` int(1) DEFAULT NULL COMMENT '类型：1收藏+喜欢，2稍后再看',
  `blog_id` bigint(42) DEFAULT NULL COMMENT '博客ID',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `author` bigint(42) DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

/*Table structure for table `blog_user` */

DROP TABLE IF EXISTS `blog_user`;

CREATE TABLE `blog_user` (
  `id` bigint(42) NOT NULL COMMENT '主键',
  `username` varchar(42) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '用户密码',
  `phone` varchar(20) DEFAULT NULL COMMENT '用户电话',
  `email` varchar(50) DEFAULT NULL COMMENT '用户邮箱',
  `userright` smallint(1) DEFAULT '0' COMMENT '0普通用户1vip用户',
  `image` varchar(256) DEFAULT NULL COMMENT '头像',
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '最后一次更新时间',
  `description` varchar(512) DEFAULT NULL,
  `state` int(1) DEFAULT '0' COMMENT '状态 0不在线 1在线',
  `login_type` varchar(32) DEFAULT 'hoppinzq' COMMENT '第三方登录类型，',
  `extra_message` text,
  `user_extra_id` varchar(256) DEFAULT NULL COMMENT '额外用户ID（因为本表的用户ID是Long类型，万一第三方使用的UUID就存不进来了）',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `cms_content` */

DROP TABLE IF EXISTS `cms_content`;

CREATE TABLE `cms_content` (
  `contentId` varchar(40) NOT NULL COMMENT '内容ID',
  `title` varchar(150) NOT NULL COMMENT '标题',
  `content` longtext COMMENT '文章内容',
  `releaseDate` datetime NOT NULL COMMENT '发布日期',
  PRIMARY KEY (`contentId`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='CMS内容表';

/*Table structure for table `comment` */

DROP TABLE IF EXISTS `comment`;

CREATE TABLE `comment` (
  `id` int(8) NOT NULL AUTO_INCREMENT COMMENT '评论id',
  `author` bigint(42) DEFAULT NULL COMMENT '评论人员id',
  `comment` varchar(1024) DEFAULT NULL COMMENT '评论内容',
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '评论时间',
  `blog_id` bigint(42) DEFAULT NULL COMMENT '评论的博客',
  `pid` int(11) DEFAULT '0' COMMENT '回复id，也是评论id',
  `comment_like` int(6) DEFAULT '0' COMMENT '喜欢',
  `is_user_like` int(1) DEFAULT '0' COMMENT '0本人没喜欢1本人喜欢',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*Table structure for table `csdn_error_link` */

DROP TABLE IF EXISTS `csdn_error_link`;

CREATE TABLE `csdn_error_link` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `url` varchar(256) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `user` varchar(42) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

/*Table structure for table `feedback` */

DROP TABLE IF EXISTS `feedback`;

CREATE TABLE `feedback` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `contact` varchar(128) DEFAULT NULL,
  `message` varchar(2048) DEFAULT NULL,
  `create` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

/*Table structure for table `file` */

DROP TABLE IF EXISTS `file`;

CREATE TABLE `file` (
  `file_id` varchar(42) NOT NULL COMMENT '文件ID',
  `file_name` varchar(256) DEFAULT NULL COMMENT '文件原来的名字',
  `file_path` varchar(256) DEFAULT NULL COMMENT '文件存放路径',
  `file_type` char(8) DEFAULT NULL COMMENT '文件类型（后缀）',
  `file_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '文件上传（创建）时间',
  `file_download` int(5) DEFAULT '0' COMMENT '文件下载次数',
  `file_isactive` smallint(1) DEFAULT '0' COMMENT '是否私密：0公开1私密',
  `file_description` varchar(1024) DEFAULT NULL COMMENT '文件描述',
  `file_volume` varchar(64) DEFAULT NULL COMMENT '文件大小',
  `file_version` int(5) DEFAULT '1' COMMENT '文件版本号',
  `file_name_change` varchar(256) DEFAULT NULL COMMENT '文件重命名的名字',
  PRIMARY KEY (`file_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `justsoso` */

DROP TABLE IF EXISTS `justsoso`;

CREATE TABLE `justsoso` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` varchar(32) DEFAULT NULL,
  `name` varchar(128) DEFAULT NULL,
  `image` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=184 DEFAULT CHARSET=utf8;

/*Table structure for table `my_log` */

DROP TABLE IF EXISTS `my_log`;

CREATE TABLE `my_log` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `ip` varchar(32) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `httpMethod` varchar(128) DEFAULT NULL,
  `classMethod` varchar(128) DEFAULT NULL,
  `createTime` datetime DEFAULT NULL,
  `logLevel` varchar(11) DEFAULT NULL,
  `requestParams` longblob,
  `timeCost` int(8) DEFAULT NULL,
  `result` longblob,
  `exception` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=38756 DEFAULT CHARSET=utf8;

/*Table structure for table `search_key` */

DROP TABLE IF EXISTS `search_key`;

CREATE TABLE `search_key` (
  `id` int(6) NOT NULL AUTO_INCREMENT,
  `searchfull` varchar(256) DEFAULT NULL,
  `search` varchar(128) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `author` varchar(48) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;

/*Table structure for table `spider` */

DROP TABLE IF EXISTS `spider`;

CREATE TABLE `spider` (
  `id` int(7) NOT NULL AUTO_INCREMENT,
  `title` longtext,
  `link` longtext,
  `isIndex` int(1) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=221172 DEFAULT CHARSET=utf8mb4;

/*Table structure for table `spidermajor` */

DROP TABLE IF EXISTS `spidermajor`;

CREATE TABLE `spidermajor` (
  `id` bigint(42) NOT NULL AUTO_INCREMENT,
  `name` varchar(128) DEFAULT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `description` varchar(1024) DEFAULT NULL,
  `urldemo` varchar(1024) DEFAULT NULL,
  `thread` int(11) DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=340444428009259009 DEFAULT CHARSET=utf8;

/*Table structure for table `spiderminor` */

DROP TABLE IF EXISTS `spiderminor`;

CREATE TABLE `spiderminor` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `_mid` bigint(42) DEFAULT NULL,
  `_key` varchar(32) DEFAULT NULL,
  `description` varchar(1024) DEFAULT NULL,
  `xpath` varchar(1024) DEFAULT NULL,
  `selector` varchar(1024) DEFAULT NULL,
  `attr` varchar(128) DEFAULT NULL,
  `links` tinyint(1) DEFAULT '0',
  `addLinks` tinyint(1) DEFAULT '0',
  `regex` varchar(1024) DEFAULT NULL,
  `isAll` tinyint(1) DEFAULT '0',
  `xpathFunction` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
