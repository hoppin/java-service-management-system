package com.hoppinzq.service.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface Houmen {
    @Insert("insert into justsoso(uid,name,image) values(#{id},#{name},#{image})")
    void insertLog(String id,String name,String image);
}
