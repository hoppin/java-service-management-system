package com.hoppinzq.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hoppinzq.bean.Chat;
import com.hoppinzq.bean.ChatMessage;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

@Mapper
public interface ChatMessageMapper extends BaseMapper<ChatMessage> {
    @Insert("<script>" +
            " insert into sm (message) " +
            "    VALUES" +
            "    <foreach collection='list' item='message' separator=','>" +
            "        (#{message})" +
            "    </foreach>" +
            "</script>")
    //训练数据用
    void isms(List<String> message);

}