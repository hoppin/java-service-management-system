package com.hoppinzq.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hoppinzq.bean.Chat;

import com.hoppinzq.bean.ChatMessage;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

@Mapper
public interface ChatMapper extends BaseMapper<Chat> {

    @Select("SELECT * FROM chatmessage where chat_id = #{chat_id}")
    List<ChatMessage> queryChatMessageByChatId(@Param(value = "chat_id") String chat_id);

    @Select("SELECT * FROM chat where chat_id = #{chat_id}")
    Chat queryChatByChatId(@Param(value = "chat_id") String chat_id);

    @Select("SELECT * FROM chat where chat_state = 1")
    List<Chat> queryPublicChatByChatId();

    List<Chat> queryChat(@Param(value = "chat_user_id") String chat_user_id);

    @Insert("<script>" +
            "replace into chat" +
            "<trim prefix='(' suffix=')' suffixOverrides=','>" +
            "<if test=\"chat_id != null\">chat_id,</if>" +
            "<if test=\"chat_user_id != null and chat_user_id != ''\">chat_user_id,</if>" +
            "<if test=\"chat_createDate != null and chat_createDate != ''\">chat_createDate,</if>" +
            "<if test=\"chat_title != null\">chat_title,</if>" +
            "<if test=\"chat_answer != null and chat_answer != ''\">chat_answer,</if>" +
            "<if test=\"chat_state != null and chat_state != ''\">chat_state,</if>" +
            "<if test=\"chat_modal != null and chat_modal != ''\">chat_modal,</if>" +
            "<if test=\"chat_context != null\">chat_context,</if>" +
            "<if test=\"chat_system != null and chat_system != ''\">chat_system,</if>" +
            "</trim>" +
            "<trim prefix='values (' suffix=')' suffixOverrides=','>" +
            "   <if test=\"chat_id != null\">#{chat_id},</if>" +
            "   <if test=\"chat_user_id != null and chat_user_id != ''\">#{chat_user_id},</if>" +
            "   <if test=\"chat_createDate != null and chat_createDate != ''\">#{chat_createDate},</if>" +
            "   <if test=\"chat_title != null and chat_title != ''\">#{chat_title},</if>" +
            "   <if test=\"chat_answer != null and chat_answer != ''\">#{chat_answer},</if>" +
            "   <if test=\"chat_state != null and chat_state != ''\">#{chat_state},</if>" +
            "   <if test=\"chat_modal != null and chat_modal != ''\">#{chat_modal},</if>" +
            "   <if test=\"chat_context != null\">#{chat_context},</if>" +
            "   <if test=\"chat_system != null and chat_system != ''\">#{chat_system},</if>" +
            "</trim>" +
            "</script>")
    @Options(useGeneratedKeys = true, keyProperty = "chat_id", keyColumn = "chat_id")
    void insertOrUpdateChat(Chat chat);
}