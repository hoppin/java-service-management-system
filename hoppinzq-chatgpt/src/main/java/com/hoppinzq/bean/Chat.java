package com.hoppinzq.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.List;

@TableName("chat")
public class Chat {

    @TableId(type = IdType.INPUT ,value = "chat_id")
    private String chat_id;
    @TableField("chat_user_id")
    private String chat_user_id;
    @TableField("chat_createDate")
    private String chat_createDate;
    @TableField("chat_title")
    private String chat_title;
    @TableField("chat_answer")
    private String chat_answer;
    @TableField("chat_state")
    private String chat_state;
    @TableField("chat_modal")
    private String chat_modal;
    @TableField("chat_context")
    private int chat_context;
    @TableField("chat_system")
    private String chat_system;

    public int getChat_context() {
        return chat_context;
    }

    public void setChat_context(int chat_context) {
        this.chat_context = chat_context;
    }

    public String getChat_system() {
        return chat_system;
    }

    public void setChat_system(String chat_system) {
        this.chat_system = chat_system;
    }

    private List<ChatMessage> chatMessageList;

    public List<ChatMessage> getChatMessageList() {
        return chatMessageList;
    }

    public void setChatMessageList(List<ChatMessage> chatMessageList) {
        this.chatMessageList = chatMessageList;
    }

    public String getChat_modal() {
        return chat_modal;
    }

    public void setChat_modal(String chat_modal) {
        this.chat_modal = chat_modal;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getChat_user_id() {
        return chat_user_id;
    }

    public void setChat_user_id(String chat_user_id) {
        this.chat_user_id = chat_user_id;
    }

    public String getChat_createDate() {
        return chat_createDate;
    }

    public void setChat_createDate(String chat_createDate) {
        this.chat_createDate = chat_createDate;
    }

    public String getChat_title() {
        return chat_title;
    }

    public void setChat_title(String chat_title) {
        this.chat_title = chat_title;
    }

    public String getChat_answer() {
        return chat_answer;
    }

    public void setChat_answer(String chat_answer) {
        this.chat_answer = chat_answer;
    }

    public String getChat_state() {
        return chat_state;
    }

    public void setChat_state(String chat_state) {
        this.chat_state = chat_state;
    }
}
