package com.hoppinzq.bean;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.hoppinzq.service.bean.RequestContext;
import com.hoppinzq.service.bean.RequestParam;

import java.util.Map;

@TableName("chatmessage")
public class ChatMessage {

    @TableId(type = IdType.INPUT,value = "message_id")
    private String message_id;
    @TableField("message_createDate")
    private String message_createDate;
    @TableField("message")
    private String message;
    @TableField("message_user_id")
    private String message_user_id;
    @TableField("message_user_name")
    private String message_user_name;
    @TableField("message_user_image")
    private String message_user_image;
    @TableField("message_role")
    private String message_role;
    @TableField("message_index")
    private String message_index;
    @TableField("chat_id")
    private String chat_id;

    public ChatMessage toMap(Map map) {
        if(map.containsKey("messageId")){
            this.message_id=map.get("messageId").toString();
        }
        if(map.containsKey("userId")){
            this.message_user_id=map.get("userId").toString();
        }
        if(map.containsKey("date")){
            this.message_createDate=map.get("date").toString();
        }
        if(map.containsKey("message")){
            this.message=map.get("message").toString();
        }
        if(map.containsKey("user")){
            this.message_user_name=map.get("user").toString();
        }
        if(map.containsKey("image")){
            this.message_user_image=map.get("image").toString();
        }
        if(map.containsKey("role")){
            this.message_role=map.get("role").toString();
        }
        if(map.containsKey("chatId")){
            this.chat_id=map.get("chatId").toString();
        }
        if(map.containsKey("index")){
            this.message_index=map.get("index").toString();
        }

        try{
            RequestParam requestParam= (RequestParam) RequestContext.getPrincipal();
            String remoteAddr = requestParam.getRequest().getRemoteAddr();
            System.out.println("userId:"+this.message_user_id+",IP:"+remoteAddr+",chatId:"+this.chat_id);
        }catch (Exception ex){

        }
        return this;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getMessage_id() {
        return message_id;
    }

    public void setMessage_id(String message_id) {
        this.message_id = message_id;
    }

    public String getMessage_createDate() {
        return message_createDate;
    }

    public void setMessage_createDate(String message_createDate) {
        this.message_createDate = message_createDate;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getMessage_user_id() {
        return message_user_id;
    }

    public void setMessage_user_id(String message_user_id) {
        this.message_user_id = message_user_id;
    }

    public String getMessage_user_name() {
        return message_user_name;
    }

    public void setMessage_user_name(String message_user_name) {
        this.message_user_name = message_user_name;
    }

    public String getMessage_user_image() {
        return message_user_image;
    }

    public void setMessage_user_image(String message_user_image) {
        this.message_user_image = message_user_image;
    }

    public String getMessage_role() {
        return message_role;
    }

    public void setMessage_role(String message_role) {
        this.message_role = message_role;
    }

    public String getMessage_index() {
        return message_index;
    }

    public void setMessage_index(String message_index) {
        this.message_index = message_index;
    }
}
