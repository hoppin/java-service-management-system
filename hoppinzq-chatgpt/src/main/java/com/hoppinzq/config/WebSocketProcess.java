package com.hoppinzq.config;

import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Controller;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArraySet;


/**
 * 保存有进程类的set
 * @ServerEndpoint 注解是一个类层次的注解，它的功能主要是将目前的类定义成一个websocket服务器端,
 * 注解的值将被用于监听用户连接的终端访问URL地址,客户端可以通过这个URL来连接到WebSocket服务器端
 * @ServerEndpoint 可以把当前类变成websocket服务类
 */
@ServerEndpoint(value = "/chatgpt/{userno}")
@Controller
public class WebSocketProcess {
    //静态变量，用来记录当前在线连接数。应该把它设计成线程安全的。
    private static int onlineCount = 0;
    private String mark="";

    //concurrent包的线程安全Set，用来存放每个客户端对应的MyWebSocket对象。
    private static CopyOnWriteArraySet<WebSocketProcess> webSocketSet = new CopyOnWriteArraySet<WebSocketProcess>();

    //与某个客户端的连接会话，需要通过它来给客户端发送数据
    private Session session;
    private String userno = "";
    /**
     * 连接建立成功调用的方法
     * */
    @OnOpen
    public void onOpen(Session session,@PathParam(value = "userno") String userno) {
        this.session = session;
        this.userno=userno;
        webSocketSet.add(this);     //加入set中
        addOnlineCount();           //在线数加1
        sendMessage("生成浏览器标识：" + userno);
    }

    /**
     * 连接关闭调用的方法
     */
    @OnClose
    public void onClose() {
        webSocketSet.remove(this);  //从set中删除
        subOnlineCount();           //在线数减1
    }


    /**
     *
     * @param message
     */
    public void sendCmdMessage(String message,String uuid){
        for (WebSocketProcess item : webSocketSet) {
            if(item.userno.equals(uuid)){
                item.sendMessage(200,message,"base");
            }else{

            }
        }
    }

    /**
     *
     * @param message
     */
    public void sendCmdMessage(int code,String message,String uuid){
        for (WebSocketProcess item : webSocketSet) {
            if(item.userno.equals(uuid)){
                item.sendMessage(code,message,"base");
            }else{

            }
        }
    }

    /**
     *
     * @param message
     */
    public void sendCmdMessage(String message,String mark,String uuid){
        for (WebSocketProcess item : webSocketSet) {
            if(item.userno.equals(uuid)){
                item.sendMessage(200,message,mark);
            }else{

            }
        }
    }

    /**
     *
     * @param message
     */
    public void sendCmdMessage(int code,String message,String mark,String uuid){
        for (WebSocketProcess item : webSocketSet) {
            if(item.userno.equals(uuid)){
                item.sendMessage(code,message,mark);
            }else{

            }
        }
    }

    public boolean closeSocket(String userno) {
        try{
            for (WebSocketProcess item : webSocketSet) {
                if(item.userno.equals(userno)){
                    item.getSession().close();
                    return true;
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
        return false;
    }

    /**
     * 收到客户端消息后调用的方法
     *
     * @param message 客户端发送过来的消息*/
    @OnMessage
    public void onMessage(String message, Session session) {
        Map map=session.getRequestParameterMap();
        String user=map.get("userno").toString().substring(1,3);
        //群发消息
        for (WebSocketProcess item : webSocketSet) {
            if(item.userno.equals(user)){
                item.sendMessage("zq:"+message);
            }
        }
    }

    @OnError
    public void onError(Session session, Throwable error) {
        error.printStackTrace();
    }


    public void sendMessage(String message){
        try{
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("code",200);
            jsonObject.put("msg",message);
            jsonObject.put("mark",mark);
            if(this.session!=null)
                this.session.getBasicRemote().sendText(jsonObject.toJSONString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void sendMessage(int code,String message){
        try{
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("code",code);
            jsonObject.put("msg",message);
            jsonObject.put("mark",mark);
            if(this.session!=null)
                this.session.getBasicRemote().sendText(jsonObject.toJSONString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void sendMessage(int code,String message,String mark){
        try{
            JSONObject jsonObject=new JSONObject();
            jsonObject.put("code",code);
            jsonObject.put("msg",message);
            jsonObject.put("mark",mark);
            if(this.session!=null)
                this.session.getBasicRemote().sendText(jsonObject.toJSONString());
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }


    /**
     * 群发自定义消息
     * */
    public static void sendInfo(String message) throws IOException {
        for (WebSocketProcess item : webSocketSet) {
            item.sendMessage(message);
        }
    }

    public static synchronized int getOnlineCount() {
        return onlineCount;
    }

    public static synchronized void addOnlineCount() {
        WebSocketProcess.onlineCount++;
    }

    public static synchronized void subOnlineCount() {
        WebSocketProcess.onlineCount--;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

}

