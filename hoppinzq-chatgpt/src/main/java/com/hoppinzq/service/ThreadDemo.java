package com.hoppinzq.service;

import com.hoppinzq.api.completion.chat.ChatCompletionRequest;
import com.hoppinzq.api.completion.chat.ChatMessage;
import com.hoppinzq.api.completion.chat.ChatMessageRole;
import lombok.SneakyThrows;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ThreadDemo implements Runnable{

    @Override
    public void run() {
        System.out.println("线程启动");

        try{
            Thread.currentThread().sleep(25000);
        }catch (Exception ex){
            ex.printStackTrace();
        }
        System.out.println("停止对话");
        final String token="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";
        final List stops = new ArrayList<>();
        stops.add("/n");
        final List<ChatMessage> messages = new ArrayList<>();
        final ChatMessage systemMessage = new ChatMessage(ChatMessageRole.USER.value(), "什么是jwt?");
        messages.add(systemMessage);
        ChatCompletionRequest chatCompletionRequest = ChatCompletionRequest
                .builder()
                .model("gpt-3.5-turbo")
                .n(1)
                .messages(messages)
                .maxTokens(50)
                .stop(stops)
                .logitBias(new HashMap<>())
                .build();
        new OpenAiService(token).createChatCompletion(chatCompletionRequest);
    }
}
