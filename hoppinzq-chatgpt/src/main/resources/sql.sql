/*
SQLyog  v12.2.6 (64 bit)
MySQL - 5.7.18-cynos-log : Database - video
*********************************************************************
*/


/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`chat` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `chat`;

/*Table structure for table `chat` */

DROP TABLE IF EXISTS `chat`;

CREATE TABLE `chat` (
    `chat_id` varchar(64) NOT NULL,
    `chat_user_id` varchar(64) DEFAULT NULL,
    `chat_createDate` varchar(128) DEFAULT NULL,
    `chat_title` varchar(128) DEFAULT NULL,
    `chat_answer` varchar(512) DEFAULT NULL,
    `chat_state` int(1) DEFAULT '0' COMMENT '0表示不公开，1表示公开',
    `chat_modal` varchar(128) DEFAULT 'gpt-3.5-turbo',
    `chat_context` int(2) DEFAULT '6',
    `chat_system` varchar(512) DEFAULT NULL COMMENT '系统角色',
    PRIMARY KEY (`chat_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `chatmessage` */

DROP TABLE IF EXISTS `chatmessage`;

CREATE TABLE `chatmessage` (
  `message_id` varchar(64) NOT NULL,
  `message_createDate` varchar(128) DEFAULT NULL,
  `message` longtext,
  `message_user_id` varchar(64) DEFAULT NULL,
  `message_user_name` varchar(64) DEFAULT NULL,
  `message_user_image` varchar(512) DEFAULT NULL,
  `message_role` varchar(64) DEFAULT NULL COMMENT 'system,user,assistant',
  `message_index` varchar(8) DEFAULT NULL,
  `chat_id` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Table structure for table `sm` */

DROP TABLE IF EXISTS `sm`;

CREATE TABLE `sm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `message` longtext,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=115 DEFAULT CHARSET=utf8;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
